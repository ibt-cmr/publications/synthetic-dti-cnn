# synthetic-dti-cnn

This repository contains the code used in the paper:

**Synthetically Trained Convolutional Neural Networks for Improved Tensor Estimation from Free-Breathing Cardiac DTI**   

The docker-container provided in the ![registry](registry.ethz.ch/ibt-cmr-public/synthetic-dti-cnn) contains all necessary dependecies to run the demonstration notebooks as well 
as all experiments presented in the paper. Furthermore a jupyter-lab installation is available inside the container. To generate 
the data-generation pipeline as well as training the network model, it is advisable to use a GPU, although as the work is implemented in 
TensorFlow 2.4 it will run on CPU too.    

## Getting started
First clone the repository and pull the docker Container from the corresponding registry: 

```
cd <path to local repository>/
git clone https://gitlab.ethz.ch/ibt-cmr-public/synthetic-dti-cnn.git
cd ./synthetic-dti-cnn

docker pull registry.ethz.ch/ibt-cmr-public/synthetic-dti-cnn
docker run -v ${pwd}:/project_repo -w /project_repo -it --rm --init -p 9000:9000 \
	registry.ethz.ch/ibt-cmr-public/synthetic-dti-cnn jupyter-lab --ip=0.0.0.0 --port=9000 --allow-root

```
Now open a Browers and copy/paste the localhost:9000 - link that is promted after starting jupyter-lab.


## Notebook order

The notebooks metioned in the Appendix start with 'Demo_...'. These are stand-alone notebooks.
To reproduce the simulation and training experiments, run the notebooks starting with 'Run_...' as follows:

1. Run_Simulation_Resource_Generation
2. Run_Simulation
3. Run_Network_Training
4. Run_Synthetic_Data_Registration
5. Run_Invivo_Lesion_Augmentation

To obtain the results and figures run the notebooks starting with 'Test_...'
1. Test_Synthetic_data
2. Test_Invivo_Data
3. Test_Augmented_Invivo_Data

The in-vivo data used for the results is not publicly shared.
