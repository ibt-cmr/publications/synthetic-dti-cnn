% addpath(genpath('../../../mutils/My/'))
% addpath(genpath('../../ptv_core_new/'))
%% git 
% addpath(genpath('../../mutils/My/'))
% addpath(genpath('../../ptv/'))
%%
% dat = load('data/ForValeriy/MRXCAT_Data.mat');
%%
% imgs0 = double(dat.data_to_save(:,:, :, 2:end, :)); % dismiss b0
% imdef = imgs0*0;
% Nslices = size(imgs0, 3);
% for i = 1 : Nslices
%     [img_def, Tmin] = register(imgs0(:,:, i, :, :));
%     imdef(:,:, i, :, :) = img_def;
% end

%% x-y
% slc = 3; 
% sz = [size(imgs0,1), size(imgs0, 2), size(imgs0,4)*size(imgs0,5)];
% implay_2d_deform(reshape(nrm(abs(squeeze(imgs0(:,:, slc, :, :)))), sz),...
%                  reshape(nrm(abs(squeeze(imdef(:,:, slc, :, :)))), sz),...
%                  squeeze(Tmin(:,:, 1, 1:2, :)), 5, 0, [0, 1]);
             
%%
% implay([reshape(nrm(abs(squeeze(imgs0(:,:, slc, :, :)))), sz), reshape(nrm(abs(squeeze(imdef(:,:, slc, :, :)))), sz)]);
%%
% savegif('anim_ex.gif',  [reshape(nrm(abs(squeeze(imgs0(:,:, slc, :, :)))), sz), reshape(nrm(abs(squeeze(imdef(:,:, slc, :, :)))), sz)], 1/20);
%%
function [imdef, T_remap] = register_python(imgs0)
sz0 = size(imgs0);
imgs0 = reshape(imgs0, sz0(1), sz0(2), 1,1, sz0(4)*sz0(5));
imgs = abs(imgs0);
imgs = imgs / max(abs(imgs(:)));
opts = [];
opts.pix_resolution = [1,1];
opts.metric = 'local_nuclear';
opts.grid_spacing = [10, 10];
opts.spline_order = 1;
opts.interp_type = 0;
opts.k_down = 0.75;
opts.display = 'off';
opts.max_iters = [100, 100, 100];
opts.border_mask = 5;
opts.check_gradients = 110*0;
opts.mean_penalty = 0.0001;
opts.local_nuclear_patch_size = 10;
opts.singular_coefs = ones(size(imgs, 5), 1);
opts.singular_coefs(1) = 0;
tic;
[voldef, Tmin3_out, ~] = ptv_register(imgs, [], opts);
toc

[T_remap, ~] = remap_displacements(Tmin3_out, 1, imgs, opts.pix_resolution);

imdef = ptv_deform(real(imgs0), T_remap, 1) + 1j * ptv_deform(imag(imgs0), T_remap, 1);
imdef = reshape(imdef, sz0); 
end