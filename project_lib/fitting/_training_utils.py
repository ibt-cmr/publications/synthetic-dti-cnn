__all__ = ['LoggingLearningRateScheduler', 'set_up_training', 'flat_tensor_to_matrix']

from typing import List, Tuple, Optional
from datetime import datetime
import os
import json

import tensorflow as tf
from cdtipy import utils
from cdtipy import tensor_metrics as tensor_evaluation


class LoggingExpLRScheduler(tf.keras.callbacks.LearningRateScheduler):
    def __init__(self, log_dir: str, init_learning_rate: float, decay_steps: int, decay_rate: float):
        schedule = tf.keras.optimizers.schedules.ExponentialDecay(init_learning_rate, decay_steps=decay_steps,
                                                                  decay_rate=decay_rate, staircase=True)
        super(LoggingExpLRScheduler, self).__init__(schedule)
        self.writer = tf.summary.create_file_writer(f'{log_dir}/train/')
        
    def on_epoch_begin(self, epoch: int, logs=None):
        super(LoggingExpLRScheduler, self).on_epoch_begin(epoch, logs)
        scheduled_lr = self.schedule(epoch)
        with self.writer.as_default():
            tf.summary.scalar('learning_rate', scheduled_lr, step=epoch)

        
def set_up_training(model: tf.keras.Model, log_root: str = None, log_dir: Optional[str] = None,
                    init_learning_rate: float = 1e-3, decay_rate: float = 0.99, decay_steps: int = 4):
    """

    """
    # Setup log-dir
    if log_dir is None:
        log_dir = f'{log_root}/{model.name}_{datetime.now().strftime("%Y%m%d-%H%M")}'
        os.makedirs(log_dir)

    callbacks = [
        LoggingExpLRScheduler(log_dir, init_learning_rate=init_learning_rate, decay_steps=decay_steps, decay_rate=decay_rate),
        tf.keras.callbacks.TensorBoard(log_dir=log_dir, profile_batch=0, write_graph=True, update_freq='epoch'),
        tf.keras.callbacks.ModelCheckpoint(f"{log_dir}/intermediate_weights", save_freq='epoch', save_weights_only=True),
    ]

    # Define optimizer
    optimizer = tf.keras.optimizers.Adam(learning_rate=init_learning_rate, amsgrad=True)
    return callbacks, log_dir, optimizer


def flat_tensor_to_matrix(flat_tensors: tf.Tensor):
    """ Reshapes a batch of vectors of length 6 (containing the 6 unique values
    of a symmetric 3x3 matrix) into square matrices

    :param flat_tensors: (..., 6) [D_xx, D_yy, D_zz, Dxy, Dxz, Dyz]
    :return: (..., 3, 3)
    """
    row_1 = tf.stack([flat_tensors[..., 0], flat_tensors[..., 3], flat_tensors[..., 4]], axis=-1)
    row_2 = tf.stack([flat_tensors[..., 3], flat_tensors[..., 1], flat_tensors[..., 5]], axis=-1)
    row_3 = tf.stack([flat_tensors[..., 4], flat_tensors[..., 5], flat_tensors[..., 2]], axis=-1)
    symmetric_matrix = tf.stack([row_1, row_2, row_3], axis=-1)
    return symmetric_matrix


def augment_tensor_with_fa_and_md(tensors: tf.Tensor):
    """
    :param: tensors - [..., 6]
    :return: augmented tensors - [..., 8]
    """
    eigen_values, _ = tf.linalg.eigh(flat_tensor_to_matrix(tensors))
    ev1, ev2, ev3 = tf.unstack(eigen_values, axis=-1)
    md = (ev1 + ev2 + ev3) / 3.
    md = tf.reduce_mean(eigen_values, axis=-1, keepdims=True)
    term1 = tf.reduce_sum(tf.pow(eigen_values - md, 2), axis=-1, keepdims=True)
    term2 = tf.reduce_sum(tf.pow(eigen_values, 2), axis=-1, keepdims=True)
    fa = tf.sqrt(3./2. * tf.math.divide_no_nan(term1, term2))
    outputs = tf.concat([tensors, md, fa], axis=-1)
    return outputs