__all__ = ['masked_mse', 'get_masked_huber', ]

from typing import List, Tuple
from datetime import datetime
import os
import json

import tensorflow as tf
import cdtipy.utils as utils
from cdtipy import tensor_metrics as tensor_evaluation
from project_lib.fitting._training_utils import flat_tensor_to_matrix


def get_masked_huber(delta=2e-3):
    l = tf.keras.losses.Huber(delta=delta, name='huber_loss')
    def masked_huber_md(y_gt: tf.Tensor, y_prediction: tf.Tensor):
        """ Assumes the labels to have the shape (..., 8)"""
        md_gt = tf.reduce_mean(y_gt[..., 0:3], axis=-1)
        mask_indices = tf.where(tf.logical_and(md_gt > 0, md_gt < 3e-3))
        masked_ygt = tf.gather_nd(y_gt, mask_indices)
        masked_ypred = tf.gather_nd(y_prediction, mask_indices)
        tensor_huber = l(masked_ygt, masked_ypred)
        md_huber = l(tf.reduce_mean(masked_ygt[..., 0:3], axis=-1), tf.reduce_mean(masked_ypred[..., 0:3], axis=-1))
        offdiag_huber = l(masked_ygt[..., 3:6],  masked_ypred[..., 3:6])
        return tensor_huber + md_huber * 3e-1 + 5e-1 * offdiag_huber
    return masked_huber_md


def masked_mse(y_gt: tf.Tensor, y_prediction: tf.Tensor,
               channel_weights: tf.Tensor = tf.ones(8, dtype=tf.float32)):
    """ Assumes the labels to have the shape (..., 8)"""
    md_gt = tf.reduce_mean(y_gt[..., 0:3], axis=-1)
    mask_indices = tf.where(tf.logical_and(md_gt > 0, md_gt < 3e-3))
    
    masked_ygt = tf.gather_nd(y_gt, mask_indices)
    masked_ypred = tf.gather_nd(y_prediction, mask_indices)
    return tf.reduce_sum(tf.pow(masked_ygt - masked_ypred, 2) * channel_weights[tf.newaxis], axis=-1)


def modified_huber(channel_weights: tf.Tensor = tf.ones(8, dtype=tf.float32), delta=2e-3):
    def loss_fn(y_gt, y_prediction):       
        with tf.device("CPU"):
            loss = tf.keras.losses.Huber(delta=delta, name='huber_loss')
            md_gt = tf.reduce_mean(y_gt[..., 0:3], axis=-1)
            mask_indices = tf.where(tf.logical_and(md_gt > 0, md_gt < 3e-3))

            masked_ygt = tf.gather_nd(y_gt, mask_indices)
            masked_ypred = tf.gather_nd(y_prediction, mask_indices)

            eigen_values, _ = tf.linalg.eigh(flat_tensor_to_matrix(masked_ypred[..., 0:6]))
            md_2 = tf.reduce_mean(eigen_values, axis=-1, keepdims=True)
            term1 = tf.reduce_sum(tf.pow(eigen_values - md_2, 2), axis=-1, keepdims=True)
            term2 = tf.reduce_sum(tf.pow(eigen_values, 2), axis=-1, keepdims=True)
            fa = tf.sqrt(3./2. * tf.math.divide_no_nan(term1, term2))

            masked_ypred = tf.concat([masked_ypred[..., 0:6], fa, masked_ypred[..., 7:]], axis=-1)
            return loss(masked_ygt, masked_ypred)
    return loss_fn


def modified_mse(channel_weights: tf.Tensor = tf.ones(8, dtype=tf.float32), delta=2e-3):
    def loss_fn(y_gt, y_prediction):       
        with tf.device("CPU"):
            md_gt = tf.reduce_mean(y_gt[..., 0:3], axis=-1)
            mask_indices = tf.where(tf.logical_and(md_gt > 0, md_gt < 3e-3))

            masked_ygt = tf.gather_nd(y_gt, mask_indices)
            masked_ypred = tf.gather_nd(y_prediction, mask_indices)

            eigen_values, _ = tf.linalg.eigh(flat_tensor_to_matrix(masked_ypred[..., 0:6]))
            md_2 = tf.reduce_mean(eigen_values, axis=-1, keepdims=True)
            term1 = tf.reduce_sum(tf.pow(eigen_values - md_2, 2), axis=-1, keepdims=True)
            term2 = tf.reduce_sum(tf.pow(eigen_values, 2), axis=-1, keepdims=True)
            fa = tf.sqrt(3./2. * tf.math.divide_no_nan(term1, term2))

            masked_ypred = tf.concat([masked_ypred[..., 0:6], fa, masked_ypred[..., 7:]], axis=-1)
            return tf.reduce_sum(tf.pow((masked_ygt - masked_ypred) * channel_weights[tf.newaxis], 2) , axis=-1)
    return loss_fn




def agp_loss_wrapper(y_gt: tf.Tensor, y_prediction: tf.Tensor):
    """ Assumes shapes (None, 7, 2) as inputs """
    return - log_agp_likelihood(y_gt[..., 0], y_prediction)


def log_agp_likelihood(label: tf.Tensor, prediction: tf.Tensor,
                       weights: Tuple[float, ...] = tuple([1., *[1. for i in range(6)]])):
    """ Calculates the log-likelihood of the amortized Gaussian Posterior.

    .. math::
        L(\Theta) = \langle \Sigma _j -\lambda_j(\mathbf{x}_j; \Theta) -
                    \\frac{(y_j - \mu_j)}{2exp(2\lambda_j(\mathbf{x}, \Theta)}
        \\rangle

    Where the index j runs over the number of parameters of the model (7, see argument y), \mu and \lambda are
    the mean and log-variance of the posterior distribution for each parameter.

    :param label: (#batch, 7) Parameter vector of the DTI - Signal representation
                       [S_0, D_xx, D_yy, D_zz, D_xy, D_xz, D_yz]
    :param prediction: (#batch, 7, 2) mean and log-variance values of the predicted by the model for each model param.
    :param weights: Weights per channel multiplied before reduce sum operation
    :return: (1, ) log-likelihood scalar value
    """
    term1 = - prediction[..., 1]
    numerator = tf.pow(label - prediction[..., 0], 2)
    denominator = 2. * tf.exp(2. * prediction[..., 1])
    value_per_parameter = term1 - tf.math.divide(numerator, denominator)
    return tf.reduce_sum(value_per_parameter, axis=-1)
