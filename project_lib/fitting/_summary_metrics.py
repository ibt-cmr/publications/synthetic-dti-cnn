__all__ = ['ValidationOnlyMetricToggle', 'MeanDiffusivityAE', 'FractionalAnisotropyAE', 'S0AE', 'TensorValueAE',
           'get_image_logging_callback']

from typing import *
from abc import abstractmethod
import sys
import io
import os

import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt

from project_lib.fitting._training_utils import flat_tensor_to_matrix
import cdtipy


class VectorMetricCallback(tf.keras.callbacks.Callback):
    def __init__(self, model: tf.keras.Model, validation_dataset: tf.data.Dataset,
                 logdir: str, metrics: Tuple[tf.keras.metrics.Metric, ...]):
        """
        
        :param: validation_dataset - batched dataset
        """
        self._model = model
        self._validation_data = validation_dataset    
        assert os.path.isdir(logdir)
        self.hist_writer = tf.summary.create_file_writer(logdir + '/only_validation')     
        self._metrics = metrics
        
    def on_epoch_end(self, epoch: int, logs=None):
        self._validate()
        with self.hist_writer.as_default():
            for m in self._metrics:
                with tf.name_scope(m.name):
                    for idx, entry in enumerate(m.result()):
                        tf.summary.scalar(m.metric_names[idx], entry, step=epoch)
    
    def _validate(self):
        for idx, (data, labels) in self._validation_data.enumerate():
            pred_raw = self._model.predict(data)
            for m in self._metrics:
                m.update_state(labels, pred_raw)

                
class MaskedMetric(tf.keras.metrics.Metric):
    def mask_md(self, y_true, y_pred):
        """
        :param y_true: (#batch, X, Y, 6)
        :param y_pred: (#batch, X, Y, 6)
        :return:
        """
        md = tf.reduce_mean(y_true[..., 0:3], axis=-1)
        masked_index = tf.where(tf.logical_and(md > 1e-5, md < 3e-3))
        y_pred = tf.gather_nd(y_pred, masked_index)
        y_true = tf.gather_nd(y_true, masked_index)
        return y_pred, y_true

    
class MaskedMDR2(tfa.metrics.RSquare, MaskedMetric):
    def __init__(self, name: str = "metrics/md_R2", dtype = None, y_shape: Tuple[int, ...] = (),
                 multioutput: str = "uniform_average", **kwargs):
        super(MaskedMDR2, self).__init__(name, dtype, y_shape, multioutput, **kwargs)
        
    def update_state(self, y_true, y_pred, sample_weight=None):
        """
        :param y_true: (#batch, X, Y, 6)
        :param y_pred: (#batch, X, Y, 6)
        :param sample_weight:
        :return:
        """
        y_pred, y_true = self.mask_md(y_true, y_pred)
        md_true = tf.reduce_mean(y_true[..., 0:3], axis=-1)
        md_pred = tf.reduce_mean(y_pred[..., 0:3], axis=-1)
        super(MaskedMDR2, self).update_state(md_true, md_pred)
    
        
class MaskedMDMAE(tf.keras.metrics.MeanAbsoluteError, MaskedMetric):
    def __init__(self, name: str = "metrics/md_MAE"):
        super(MaskedMDMAE, self).__init__(name)
        
    def update_state(self, y_true, y_pred, sample_weight=None):
        """
        :param y_true: (#batch, X, Y, 6)
        :param y_pred: (#batch, X, Y, 6)
        :param sample_weight:
        :return:
        """
        y_pred, y_true = self.mask_md(y_true, y_pred)
        md_true = tf.reduce_mean(y_true[..., 0:3], axis=-1)
        md_pred = tf.reduce_mean(y_pred[..., 0:3], axis=-1)
        super(MaskedMDMAE, self).update_state(md_true, md_pred)
        
        
class MaskedFAR2(tfa.metrics.RSquare, MaskedMetric):
    def __init__(self, name: str = "metrics/fa_R2", dtype = None, y_shape: Tuple[int, ...] = (),
                 multioutput: str = "uniform_average", **kwargs):
        super().__init__(name, dtype, y_shape, multioutput, **kwargs)
        
    def update_state(self, y_true, y_pred, sample_weight=None):
        """
        :param y_true: (#batch, X, Y, 8)
        """
        with tf.device("CPU"):
            y_pred, y_true = self.mask_md(y_true, y_pred)

            eigen_values, _ = tf.linalg.eigh(flat_tensor_to_matrix(y_pred[..., 0:6]))
            md_2 = tf.reduce_mean(eigen_values, axis=-1, keepdims=True)
            term1 = tf.reduce_sum(tf.pow(eigen_values - md_2, 2), axis=-1, keepdims=True)
            term2 = tf.reduce_sum(tf.pow(eigen_values, 2), axis=-1, keepdims=True)
            fa = tf.sqrt(3./2. * tf.math.divide_no_nan(term1, term2))
            
        super().update_state(y_true[..., 6], fa[..., 0])
    
        
class MaskedFAMAE(tf.keras.metrics.MeanAbsoluteError, MaskedMetric):
    def __init__(self, name: str = "metrics/fa_MAE"):
        super().__init__(name)
        
    def update_state(self, y_true, y_pred, sample_weight=None):
        """
        :param y_true: (#batch, X, Y, 8)
        :return:
        """
        y_pred, y_true = self.mask_md(y_true, y_pred)
        super().update_state(y_true[..., 6], y_pred[..., 6])
        
        
class MaskedOffdiagMAE(tf.keras.metrics.MeanAbsoluteError, MaskedMetric):
    def __init__(self, name: str = "metrics/offdiag_MAE"):
        super(MaskedOffdiagMAE, self).__init__(name)
        
    def update_state(self, y_true, y_pred, sample_weight=None):
        """
        :param y_true: (#batch, X, Y, 6)
        :param y_pred: (#batch, X, Y, 6)
        :param sample_weight:
        :return:
        """
        y_pred, y_true = self.mask_md(y_true, y_pred)
        offdiag_true = tf.reduce_mean(y_true[..., 3:6], axis=-1)
        offdiag_pred = tf.reduce_mean(y_pred[..., 3:6], axis=-1)
        super(MaskedOffdiagMAE, self).update_state(offdiag_true, offdiag_pred)
             
        
class MaskedTensorAE(tf.keras.metrics.MeanTensor, MaskedMetric):
    def __init__(self, name='metrics/fullTensorMAE', dtype=None):
        super(MaskedTensorAE,  self).__init__(name, dtype)
    
    def update_state(self, y_true, y_pred, sample_weight=None):
        """
        :param y_true: (#batch, X, Y, 6)
        :param y_pred: (#batch, X, Y, 6)
        :param sample_weight:
        :return:
        """
        y_pred, y_true = self.mask_md(y_true[..., 0:6], y_pred[..., 0:6])
        count_add = tf.shape(y_true)[0]
        summed_ae = tf.reduce_sum(tf.abs(y_true - y_pred), axis=0)
        super(MaskedTensorAE, self).update_state(summed_ae)
        count = self._count.read_value()
        self._count.assign(count + tf.cast(-1 + count_add, tf.float32))                
            

def get_metric_map_logging_callback(model: tf.keras.Model, validation_dataset: tf.data.Dataset, image_file_writer=None,
                               logdir: str = None, n_images: int = 1, frequency: int = 10):
    
    if image_file_writer is None:
        image_file_writer = tf.summary.create_file_writer(logdir + '/images')

    def _get_images(data, flat_values_label, flat_values_pred):
        md = tf.reduce_mean(flat_values_pred[0, :, :, 0:3], axis=-1, keepdims=False)
        md_gt = tf.reduce_mean(flat_values_label[0, :, :, 0:3], axis=-1, keepdims=False)
        md_diff = tf.abs(md - md_gt)
        
        tensors = cdtipy.utils.flat_tensor_to_matrix(flat_values_pred[0, ..., :])
        evs = tf.linalg.eigvalsh(tensors)
        fa = cdtipy.tensor_metrics.fractional_anisotropy(evs)
        
        tensors_gt = cdtipy.utils.flat_tensor_to_matrix(flat_values_label[0, ..., :])
        evs_gt = tf.linalg.eigvalsh(tensors_gt)
        fa_gt = cdtipy.tensor_metrics.fractional_anisotropy(evs_gt)
        fa_diff = tf.abs(fa - fa_gt)
        evs_diff = tf.abs(evs_gt - evs)
        
        gt = dict(MD=md_gt, FA=fa_gt, EV1=evs_gt[..., 2], EV2=evs_gt[..., 1], EV3=evs_gt[..., 0])
        pred = dict(MD=md, FA=fa, EV1=evs[..., 2], EV2=evs[..., 1], EV3=evs[..., 0])
        diff = {k: np.abs(v - gt[k]) for k, v in pred.items()}
        input_images = np.array(data[0, ..., 0:5]).transpose([2, 0, 1])
        
        return plot_to_image(plot_metric_maps(input_images, pred, gt, diff))

    def log_random_validation_inference(epoch, logs):
        if epoch % frequency == 0:
            with image_file_writer.as_default():
                for idx, (data, labels) in validation_dataset.shuffle(buffer_size=10).take(n_images).enumerate():
                    pred_raw = model.call(data)
                    image = _get_images(data, labels, pred_raw)
                    tf.summary.image(f"Metrics {int(idx)}", image, step=epoch)

    return tf.keras.callbacks.LambdaCallback(on_epoch_end=log_random_validation_inference)



def get_lesion_map_logging_callback(model: tf.keras.Model, validation_dataset: tf.data.Dataset, image_file_writer=None,
                                   logdir: str = None, n_images: int = 5, frequency: int = 10):
     
    if image_file_writer is None:
        image_file_writer = tf.summary.create_file_writer(logdir + '/images')

    def log_random_validation_inference(epoch, logs):
        if epoch % frequency == 0:
            with image_file_writer.as_default():
                data, labels = [i for i in validation_dataset.shuffle(buffer_size=20).batch(n_images).take(1)][0]
                prediction = model.predict(data)
                image = plot_to_image(plot_lesion_maps(np.array(data[..., 0]), np.array(prediction[..., 6:9]), np.array(labels[..., 6:9])))
                tf.summary.image(f"Lesion Maps", image, step=epoch)

    return tf.keras.callbacks.LambdaCallback(on_epoch_end=log_random_validation_inference)



def plot_metric_maps(input_images: np.ndarray, network_inf: dict = None, gt: dict = None, diff: dict = None):
    
    fig, axes = plt.subplots(4, len(list(gt.keys())))
    if len(list(gt.keys()))==1:
        axes = [axes, ]
        
    cbar_intervals = [
        {'MD': (0.5e-3, 2.5e-3), 'FA': (0.15, 0.6), 'EV1': (0.5e-3, 2.5e-3), 'EV2': (0.5e-3, 2.5e-3), 'EV3': (0.5e-3, 2.5e-3)},
        {'MD': (0.8e-3, 2.1e-3), 'FA': (0.15, 0.6), 'EV1': (0.5e-3, 2.5e-3), 'EV2': (0.5e-3, 2.5e-3), 'EV3': (0.5e-3, 2.5e-3)},
        {'MD': (0., 0.3e-4), 'FA': (0., 0.1), 'EV1': (0., 0.3e-3), 'EV2': (0., 0.3e-3), 'EV3': (0., 0.3e-3)},
    ]
    
    for col_idx, ax in enumerate(axes[0]):       
        md_gt0 = np.ma.masked_less(np.where(gt['MD'] > 0., np.ones_like(gt['MD']), np.zeros_like(gt['MD'])), 0.5)
        ax.set_xticks([]), ax.set_yticks([])
        im = ax.imshow(input_images[col_idx], cmap='gray')
        ax.imshow(md_gt0, vmin=0, vmax=1, alpha=0.5)
        cbar = fig.colorbar(im, ax=ax, fraction=0.043, pad=0.04 )
        cbar.minorticks_on(), cbar.formatter.set_powerlimits((0, 0))
    
    for ax_row, metric_dict, intervals in zip(axes[1:], [gt, network_inf, diff], cbar_intervals):
        for ax, (name, metric_map) in zip(ax_row, metric_dict.items()):
            
            vmin, vmax = intervals[name]
            im = ax.imshow(metric_map, cmap='plasma', vmin=vmin, vmax=vmax)
            ax.set_xticks([]), ax.set_yticks([])
            cbar = fig.colorbar(im, ax=ax, fraction=0.045, pad=0.04 )
            cbar.minorticks_on(), cbar.formatter.set_powerlimits((0, 0))
    
    
    for ax, title in zip(axes[1], gt.keys()):
        ax.set_title(title, fontweight='bold', fontsize=25)
    
    for rowidx, title in enumerate(["Input", "GT", "Prediction", "$|\Delta|$"]):
        axes[rowidx, 0].set_ylabel(title, fontsize=25, fontweight="bold")    
        
    fig.set_size_inches(5*len(ax_row), len(axes)*5)
    fig.subplots_adjust(left=0.05, top=0.95, bottom=0.02, right=0.95, wspace=0.1, hspace=0.03)
    return fig


def plot_lesion_maps(input_images: np.ndarray, network_infs: np.ndarray, labels: np.ndarray):
    """
    :param input_images: [b, X, Y]
    :param network_inf: [b, X, Y, 3]
    :param labels: [b, X, Y, 3]
    """
    col_stacked_data = np.concatenate([*input_images], axis=1)
    col_stacked_pred = np.concatenate([*network_infs], axis=1)
    col_stacked_label = np.concatenate([*labels], axis=1)
    
    fig, axes = plt.subplots(4, 1)
    [(a.set_xticks([]), a.set_yticks([])) for a in axes]
    axes[0].imshow(col_stacked_data, cmap='gray')
    axes[0].imshow(np.argmax(col_stacked_label, axis=-1), cmap='jet', vmin=0, vmax=2, alpha=0.5)
    axes[0].set_ylabel("Reference", fontsize=25, fontweight="bold")  
    
    for class_idx in range(3):
        axes[class_idx+1].imshow(col_stacked_data, cmap='gray')
        im = axes[class_idx+1].imshow(col_stacked_pred[..., class_idx], cmap='hot', vmin=0, vmax=1, alpha=0.5)
#         cbar = fig.colorbar(im, ax=axes[class_idx+1], fraction=0.045, pad=0.04)
#         cbar.minorticks_on(), cbar.formatter.set_powerlimits((0, 1))
        axes[class_idx+1].set_ylabel(f"Softmax channel {class_idx}", fontsize=25, fontweight="bold")
    
    fig.set_size_inches(5*input_images.shape[0], len(axes)*5)
    fig.subplots_adjust(left=0.05, top=0.95, bottom=0.02, right=0.95, hspace=0.0)
    return fig


def plot_to_image(figure, channels=3):
    """Converts the matplotlib plot specified by 'figure' to a PNG image and
    returns it. The supplied figure is closed and inaccessible after this call."""
    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    # Closing the figure prevents it from being displayed directly
    plt.close(figure)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=channels)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image


