__all__ = ["FullyConnectedNet", "SimpleConvnet"]

from typing import Tuple, List, Union, Optional
import tensorflow as tf


class FullyConnectedNet(tf.keras.Model):
    def __init__(self, n_layers: int = 5, n_neurons: int = 50, kernel_reg: float = 0.01,
                 activity_reg: float = 0.01, dropout_rate: float = 0.):
        super(FullyConnectedNet, self).__init__()
        self.hidden_layers = [tf.keras.layers.Dense(n_neurons,
                                                    kernel_regularizer=tf.keras.regularizers.l1(kernel_reg),
                                                    activity_regularizer=tf.keras.regularizers.l2(activity_reg)) for _
                              in range(n_layers)]

    def call(self, inputs, training=None, mask=None):
        """
        :param inputs: (#batch, 13*12)
        :param training:
        :param mask:
        :return: (#batch, 7, 2)
        """
        x = inputs
        for h_layer in self.hidden_layers:
            x = h_layer(x)
            x = tf.keras.activations.relu(x, alpha=0.1)
        return x


class SimpleConvnet(tf.keras.Model):
    def __init__(self, filters_in_blocks: Tuple[int, ...] = (16, 32, 64), layers_per_block: int = 3,
                 dropout_rate: float = 0.03,  input_shape: Tuple = (None, 13), out_mean=None, out_std=None,
                 output_channels: int = 1, **kwargs):
        super(SimpleConvnet, self).__init__()
        blocks = []
        for block_index, n_filters in enumerate(filters_in_blocks):
            blocks += [tuple([tf.keras.layers.Conv2D(n_filters, kernel_size=3, padding='SAME')
                             for _ in range(layers_per_block)]), ]
        self.dropouts = tuple([tf.keras.layers.Dropout(rate=dropout_rate) for _ in range(len(filters_in_blocks))])
        self.blocks = tuple(blocks)
        self.output_layer = tf.keras.layers.Conv2D(output_channels, kernel_size=1, strides=1, padding='SAME')
        self.out_mean = out_mean[tf.newaxis, tf.newaxis, tf.newaxis]
        self.out_std = out_std[tf.newaxis, tf.newaxis, tf.newaxis]
        self.build(input_shape=input_shape)

    def call(self, inputs, training=None, mask=None):
        x = inputs
        for block, dropout in zip(self.blocks, self.dropouts):
            for layer in block:
                x = layer(x)
                x = tf.keras.activations.relu(x, alpha=0.1)
            x = dropout(x, training=training)
        out = self.output_layer(x)
        out = out * self.out_std + self.out_mean
        return out