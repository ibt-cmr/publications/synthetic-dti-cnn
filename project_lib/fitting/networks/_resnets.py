__all__ = ["XceptionResnet", "InceptionResnet"]

from typing import Tuple, List, Union, Optional
import tensorflow as tf


def XceptionResnet(initial_block_filters: Tuple[int, ...] = (32, 64, 128), head_depth_multipliers=(16, 8, 8), 
                   number_of_res_blocks: int = 3, res_block_filters: Tuple[int, ...] = (16, 16, 16), 
                   dropout_rate: float = 0.05, last_kernel_size: int = 3, out_mean=None, out_std=None,
                   input_shape=(None, None, None, 13), output_channels: int = 1, **kwargs):
    """ Defines a tf.keras.Model instance of type Inception ResNet, consisting of a header block of a conv2D layes
    and a specified amount of residual blocks. Each residual inception block consist of three channels (1, 3, 3-dilated).
    
    :returns: tf.keras.Model
    """
    inputs = tf.keras.Input(shape=input_shape[1:])
    
    x = inputs
    for f, dm in zip(initial_block_filters, head_depth_multipliers):
        x = tf.keras.layers.SeparableConv2D(f, kernel_size=3, depth_multiplier=dm, padding="SAME")(x)
        x = tf.keras.layers.ReLU(negative_slope=0)(x)
    x = tf.keras.layers.Dropout(dropout_rate)(x)
    
    for residual_block_index in range(number_of_res_blocks):
        x1, x3, x3_dilated = x, x, x
        resblock_input = x
        for f in res_block_filters:
            x1 = tf.keras.layers.Conv2D(f, kernel_size=1, padding="SAME")(x1)
            x1 = tf.keras.layers.ReLU(negative_slope=0.1)(x1)
            x3 = tf.keras.layers.Conv2D(f, kernel_size=3, padding="SAME")(x3)
            x3 = tf.keras.layers.ReLU(negative_slope=0.1)(x3)
            x3_dilated = tf.keras.layers.Conv2D(f, kernel_size=3, padding="SAME", dilation_rate=2)(x3_dilated)
            x3_dilated = tf.keras.layers.ReLU(negative_slope=0.1)(x3_dilated)
            
        resblock_out = tf.concat([x1, x3, x3_dilated], axis=-1)
        x = tf.keras.layers.Conv2D(initial_block_filters[-1], 1, padding="SAME")(resblock_out)
        x = tf.keras.layers.ReLU(negative_slope=0.1)(x)
        x = tf.keras.layers.Add()([resblock_input, x])
    x = tf.keras.layers.Dropout(dropout_rate)(x)
        
    # Add output channel per regression output, and scale the output
    outputs = tf.keras.layers.Conv2D(output_channels, last_kernel_size, padding="same",
                                     activation=None)(x)
    outputs = outputs * out_std + out_mean
    
    # Define the model
    model = tf.keras.Model(inputs, outputs, name="xception_resnet")
    return model


def InceptionResnet(initial_block_filters: Tuple[int, ...] = (32, 64, 128),
                    dropout_rate: float = 0.05, number_of_res_blocks: int = 3,
                    res_block_filters: Tuple[int, ...] = (16, 16, 16),
                    input_shape=(None, None, None, 13), out_mean=None,
                    out_std=None, output_channels: int = 1,
                    last_kernel_size: int = 3, **kwargs):
    """ Defines a tf.keras.Model instance of type Inception ResNet, consisting of a header block
    of a conv2D layes and a specified amount of residual blocks. Each residual inception block
     consist of three channels (1, 3, 3-dilated).
    
    :returns: tf.keras.Model
    """
    inputs = tf.keras.Input(shape=input_shape[1:])
    
    x = inputs
    for f in initial_block_filters:
        x = tf.keras.layers.Conv2D(f, kernel_size=3, padding="SAME")(x)
        x = tf.keras.layers.ReLU(negative_slope=0.1)(x)
    x = tf.keras.layers.Dropout(dropout_rate)(x)
    
    for residual_block_index in range(number_of_res_blocks):
        x1, x3, x3_dilated = x, x, x
        resblock_input = x
        for f in res_block_filters:
            x1 = tf.keras.layers.Conv2D(f, kernel_size=1, padding="SAME")(x1)
            x1 = tf.keras.layers.ReLU(negative_slope=0.1)(x1)
            x3 = tf.keras.layers.Conv2D(f, kernel_size=3, padding="SAME")(x3)
            x3 = tf.keras.layers.ReLU(negative_slope=0.1)(x3)
            x3_dilated = tf.keras.layers.Conv2D(f, kernel_size=3, padding="SAME",
                                                dilation_rate=2)(x3_dilated)
            x3_dilated = tf.keras.layers.ReLU(negative_slope=0.1)(x3_dilated)
            
        resblock_out = tf.concat([x1, x3, x3_dilated], axis=-1)
        x = tf.keras.layers.Conv2D(initial_block_filters[-1], 1, padding="SAME")(resblock_out)
        x = tf.keras.layers.ReLU(negative_slope=0.1)(x)
        x = tf.keras.layers.Add()([resblock_input, x])
    x = tf.keras.layers.Dropout(dropout_rate)(x)
        
    # Add output channel per regression output, and scale the output
    unscaled_out = tf.keras.layers.Conv2D(output_channels, last_kernel_size, padding="same",
                                          activation=None)(x)
    tensor_out = unscaled_out[..., :6] * out_std[:6] + out_mean[:6]
    md = tf.reduce_mean(tensor_out[..., 0:3], axis=-1, keepdims=True)

    outputs = tf.concat([tensor_out, tf.zeros_like(md), md], axis=-1)
    
    # Define the model
    model = tf.keras.Model(inputs, outputs, name="inception_resnet")
    return model
