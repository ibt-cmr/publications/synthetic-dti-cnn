__all__ = ["UNet", ]

from typing import Tuple, List, Union, Optional
import tensorflow as tf


def UNet(initial_block_filters=(32, 64), down_stream_filters=(64, 128), upstream_filters=(64, 32), dropout_rate=0.1,
         input_shape=(None, None, None, 144), output_channels=6, out_mean=None, out_std=None, 
         last_kernel_size: int = 3, **kwargs):
    """ Definition of Unet with variable number of Output channels
    
    :returns: tf.keras.Model 
    """ 
        
    inputs = tf.keras.Input(shape=input_shape[1:])
        
    # Entry block
    x = inputs
    for f in initial_block_filters:
        x = tf.keras.layers.Conv2D(f, 3, strides=1, padding="same", activation=tf.keras.activations.relu)(x)
    
    # Set aside skip connections to concatenate to the upstream block
    skip_connections = []
    
    # Downstream block
    for filters in down_stream_filters[:-1]:
        skip_connections.insert(0, x)
        x = tf.keras.layers.MaxPooling2D(3, strides=2, padding="same")(x)
        x = tf.keras.layers.Conv2D(filters, 3, padding="same", activation=tf.keras.activations.relu)(x)
        x = tf.keras.layers.Conv2D(filters, 3, padding="same", activation=tf.keras.activations.relu)(x)
    
    # Lowest spatial resolution
    x = tf.keras.layers.Conv2D(down_stream_filters[-1], 3, padding="same", 
                               activation=tf.keras.activations.relu)(x)
    x = tf.keras.layers.Conv2D(down_stream_filters[-1], 3, padding="same", 
                               activation=tf.keras.activations.relu)(x)

    ### Upstream block
    for filters, skip_layer in zip(upstream_filters, skip_connections):
        x = tf.keras.layers.Conv2DTranspose(filters, 3, strides=2, padding="same",
                                            activation=tf.keras.activations.relu)(x)
        x = tf.concat([skip_layer, x], axis=-1)
        print(skip_layer.shape, x.shape)
        x = tf.keras.layers.Conv2D(filters, 3, padding="same", 
                                   activation=tf.keras.activations.relu)(x)
        x = tf.keras.layers.Conv2D(filters, 3, padding="same", 
                                   activation=tf.keras.activations.relu)(x)
    x = tf.keras.layers.Dropout(dropout_rate)(x)
        
    # Add output channel per regression output, and scale the output
    outputs = tf.keras.layers.Conv2D(output_channels, last_kernel_size, padding="same", activation=None)(x)
    outputs = outputs * out_std + out_mean
    
    # Define the model
    model = tf.keras.Model(inputs, outputs, name="unet")
    return model
