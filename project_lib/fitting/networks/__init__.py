""" Module including a zoo of models, that are used to train multi"""

from project_lib.fitting.networks._resnets import *
from project_lib.fitting.networks._unets import *
from project_lib.fitting.networks._simple_reference import *
