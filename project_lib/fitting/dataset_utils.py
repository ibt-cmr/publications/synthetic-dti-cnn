""" Module containing functions that instantiate tf.data.Datasets for
specific scenarios used for training and data inspection """

from typing import List, Tuple
import json
import os
from glob import glob

import tensorflow as tf

from skimage.morphology import dilation, disk
import numpy as np
# from project_lib.fitting import records_io


def setup_split_dataset(snr: str, motion: str, resource_dir: str, split_index: int = -10, shuffle_seed: int = 0,
                        dataset_function: callable = None, n_averages: int = None, dataset_kwargs: dict = None, use_first_n_files: int = None):
    """ Searches resource dir for files, matching the dataset format and returns
    a split data set (training / validation) according to the handed argument
    dataset_function

    :param snr: (str)  used to compose directory to load
    :param motion: (str)  used to compose directory to load
    :param resource_dir: (str)  used to compose directory to load
    :param split_index: (int) < #Examples in directory
    :param shuffle_seed: (int) used to randomize data order
    :param dataset_function: (callable) from this module to instantiate a tf Dataset
    :param n_averages: (int) #Averages that should be used (<= #Available avgs in data)
    :param dataset_kwargs: (dict) keyword arguments that are passed to dataset_function
    :return: train_dataset, train_mask, valid_dataset, valid_mask, input_shape -
             (Dataset, Dataset, Dataset, Dataset, Tuple)
    """
    with open(f'{resource_dir}/parameters.json', 'r') as p:
        parameters = json.load(p)['example_offset_0']

    if n_averages is None:
        n_averages = parameters['n_averages']
    n_bvectors = len(parameters['b_vectors'])
    
    
    data_samples = glob(f"{resource_dir}/labels/label_*.npz")
    index_list = [int(os.path.basename(f)[6:10]) for f in data_samples]
    index_list.sort()
    
    print(f'Files found in specified location: {len(index_list)}')
    if use_first_n_files is not None:
        number_files = use_first_n_files
        print(f'Using first {number_files} of them')
    else:
        number_files = len(index_list)
        
    data_filenames = [f"{resource_dir}/snr_{snr}/{motion}_images_{i:04}.npz" for i in index_list[0:number_files]]
    labels_filenames = [f"{resource_dir}/labels/label_{i:04}.npz" for i in index_list[0:number_files]] 
    mask_filenames = [f"{resource_dir}/labels/clean_mask_{i:04}.npz" for i in index_list[0:number_files]]
    random_indices = tf.random.shuffle(tf.range(len(data_filenames)), seed=shuffle_seed).numpy()
    
    train_x_files, train_y_files, train_m_files = ([data_filenames[i] for i in random_indices[0:split_index]],
                                                   [labels_filenames[i] for i in random_indices[0:split_index]],
                                                   [mask_filenames[i] for i in random_indices[0:split_index]])
    train_dataset, train_mask = dataset_function(train_x_files, train_y_files, train_m_files, n_averages, n_bvectors,
                                                 **dataset_kwargs)

    valid_x_files, valid_y_files, valid_m_files = ([data_filenames[i] for i in random_indices[split_index:]],
                                                   [labels_filenames[i] for i in random_indices[split_index:]],
                                                   [mask_filenames[i] for i in random_indices[split_index:]])

    valid_dataset, valid_mask = dataset_function(valid_x_files, valid_y_files, valid_m_files, n_averages, n_bvectors,
                                                 **dataset_kwargs)
    
    input_shape = (None, None, None, n_averages*(n_bvectors))
    return train_dataset, train_mask, valid_dataset, valid_mask, input_shape, number_files


def get_dataset_gt(gt_files: List[str], flatten_output: bool = False, normalize_s0: bool = True):
    """

    :param gt_files:
    :param flatten_output:
    :param normalize_s0:
    :return:
    """
    def normalize_gt(y):
        mask = tf.where(y[..., 0] > 0, tf.ones_like(y[..., 0]), tf.zeros_like(y[..., 0]))
        mean_s0 = tf.reduce_sum(y[..., 0], keepdims=False) / tf.reduce_sum(mask)
        scale = tf.concat([mean_s0[tf.newaxis], tf.constant([1, 1, 1, 1, 1, 1], dtype=tf.float32)], axis=0)
        return y / scale

    gt = tf.data.TFRecordDataset(filenames=gt_files).map(records_io.parse_labels)
    if normalize_s0:
        gt = gt.map(normalize_s0)
    return gt


def get_dataset_1d(data_files: List[str], label_files: List[str], mask_files: List[str], n_averages: int,
                   n_bvalues: int) -> (tf.data.Dataset, tf.data.Dataset):
    """ Constructed dataset returns only the data inside the masked lv as
    flat vector

    :param data_files: List[str]
    :param label_files: List[str]
    :param mask_files: List[str]
    :param n_averages: (int) #Averages to be used <= #Available averages
    :param n_bvalues: (int) #Diffusion weightings used to simulate the data
    :return: data, masks - (Dataset, Dataset)
    """
    def _flatten_outputs(data_and_labels, mask):
        x, y = data_and_labels
        mask_indices = tf.where(mask > 0.5)
        flat_data = tf.gather_nd(x, mask_indices)
        flat_labels = tf.gather_nd(y, mask_indices)
        return flat_data, flat_labels

    dataset_2d, masks = get_dataset_2d(data_files, label_files, mask_files, n_averages, n_bvalues,
                                       mask_left_ventricle=False, normalize_to_s0=True, blur_mask=False)
    dataset_1d = tf.data.Dataset.zip((dataset_2d, masks)).map(_flatten_outputs)
    return dataset_1d, masks


def get_dataset_2d(data_files: List[str], label_files: List[str], mask_files: List[str], n_averages: int,
                   n_bvalues: int, mask_left_ventricle: bool = True, normalize_to_s0: bool = True,
                   blur_mask: bool = True, return_complex: bool = False, mask_dilation: float = 0.,
                   crop_to_box: Tuple[int, int, int, int] = None) -> (tf.data.Dataset, tf.data.Dataset):
    """

 :param data_files: List[str]
    :param label_files: List[str]
    :param mask_files: List[str]
    :param n_averages: (int) #Averages to be used <= #Available averages
    :param n_bvalues: (int) #Diffusion weightings used to simulate the data
    :param mask_left_ventricle: (bool) if yes - masks the data to initial mask
    :param normalize_to_s0: (bool) if yes - normalizes the magnitude data to the
                                average signal in the first image
    :param blur_mask: (bool) if yes - smoothes the mask prior to masking with a kernel
                                        of size 3 to prevent too sharp edges
    :param return_complex: if yes - returns complex instead of magnitude data
    :param mask_dilation: float Radius of the disk-structured element that is use for dilation of the 
                            mask used for masking the data. The return mask dataset (1) is not affected
                            by this dilation.
    :param crop_to_box: (offset_row, offset_col, target_height, target_width]
    :return:
    """
    assert data_files and len(data_files) == len(label_files)

    def smooth_mask(single_mask):
        x = tf.range(-1, 2, dtype=tf.float32) ** 2
        z = tf.exp(-x / (2.0 * 0.5 ** 2))
        filter_kernel1d = z / tf.reduce_sum(z)
        filter_kernel2d = tf.einsum('x, y -> xy', filter_kernel1d, filter_kernel1d)[:, :, tf.newaxis, tf.newaxis]
        return tf.nn.conv2d(single_mask[tf.newaxis, :, :, tf.newaxis], filter_kernel2d, 1, padding='SAME')[0, ..., 0]

    def mask_2d(x, single_mask, binary: bool = False):
        single_mask = tf.where(single_mask > 0.1, tf.ones_like(single_mask), tf.zeros_like(single_mask))
        single_mask = tf.cast(single_mask, x.dtype)
        if binary:
            single_mask = tf.round(single_mask)
        return tf.einsum('xy..., xy -> xy...', x, single_mask)

    def normalize_data(x, single_mask):
        """ Normalizes the signal of all differently diffusion weighted images, by the mean absolute signal of the 
        first image / average inside the left-ventricle mask.
        """
        binary_mask = tf.round(single_mask)
        masked_mean = tf.reduce_sum(binary_mask * tf.abs(x[..., 0, 0])) / tf.reduce_sum(binary_mask)
        return x / tf.complex(masked_mean, 0.)

    def normalize_labels(y, single_mask):
        binary_mask = tf.round(single_mask)
        mean_s0 = tf.reduce_sum(y[..., 0], keepdims=False) / tf.reduce_sum(binary_mask)
        scale = tf.concat([mean_s0[tf.newaxis], tf.constant([1, 1, 1, 1, 1, 1], dtype=tf.float32)], axis=0)
        return y / scale

    def crop(x):
        return x[crop_to_box[0]:crop_to_box[0] + crop_to_box[2],
               crop_to_box[1]:crop_to_box[1] + crop_to_box[3]]

    def dilate(x, r):
        """Warning dilates lesions as well..."""
        np_str_element = tf.constant(disk(r), dtype=tf.float32)[..., tf.newaxis]
        str_element = tf.where(np_str_element > 0, tf.zeros_like(np_str_element),
                               - tf.constant(np.inf) * tf.zeros_like(np_str_element))
        return tf.nn.dilation2d(x[tf.newaxis, ..., tf.newaxis], str_element, [1, 1, 1, 1], padding='SAME',
                                data_format="NHWC", dilations=[1, 1, 1, 1])[0, ..., 0]

    def erode(x, r):
        """Warning erodes lesions as well..."""
        np_str_element = tf.constant(disk(r), dtype=tf.float32)[..., tf.newaxis]
        str_element = tf.where(np_str_element > 0, tf.zeros_like(np_str_element),
                               - tf.constant(np.inf) * tf.zeros_like(np_str_element))
        return tf.nn.erosion2d(x[tf.newaxis, ..., tf.newaxis], str_element, [1, 1, 1, 1], padding='SAME',
                               data_format="NHWC", dilations=[1, 1, 1, 1])[0, ..., 0]


    def _load_np_data(string_tensor):
        return np.load(string_tensor.numpy().decode())["image"]

    data = tf.data.Dataset.from_tensor_slices(tf.constant(data_files, dtype=tf.string))
    data = data.map(lambda x: tf.py_function(_load_np_data, [x], tf.complex64))

    def _load_np_label(string_tensor):
        return np.load(string_tensor.numpy().decode())["label"]

    labels = tf.data.Dataset.from_tensor_slices(tf.constant(label_files, dtype=tf.string))
    labels = labels.map(lambda x: tf.py_function(_load_np_label, [x], tf.float32))

    def _load_np_mask(string_tensor):
        return np.load(string_tensor.numpy().decode())["mask"]

    masks = tf.data.Dataset.from_tensor_slices(tf.constant(mask_files, dtype=tf.string))
    masks = masks.map(lambda x: tf.py_function(_load_np_mask, [x], tf.float32))

    lv_mask = masks.map(lambda x: tf.where(x > 0.1, tf.ones_like(x), tf.zeros_like(x)))

    if mask_dilation > 0:
        lv_mask = lv_mask.map(lambda x: dilate(x, mask_dilation))

    if crop_to_box is not None:
        data = data.map(crop)
        labels = labels.map(crop)
        masks = masks.map(crop)
        lv_mask = lv_mask.map(crop)

    if blur_mask:
        lv_mask = lv_mask.map(smooth_mask)

    if mask_left_ventricle:
        data = tf.data.Dataset.zip((data, lv_mask)).map(mask_2d)
        labels = tf.data.Dataset.zip((labels, lv_mask)).map(mask_2d)

    if normalize_to_s0:
        eroded_masks = masks.map(lambda x: erode(x, 2))
        data = tf.data.Dataset.zip((data, eroded_masks)).map(normalize_data)
    #         labels = tf.data.Dataset.zip((labels, masks)).map(normalize_labels)  # unnecessary for data without s0

    if not return_complex:
        data = data.map(tf.abs)

    return tf.data.Dataset.zip((data, labels)), masks


def get_scalar_dataset_2d(data_files: List[str], label_files: List[str], mask_files: List[str], n_averages: int,
                          n_bvalues: int, mask_left_ventricle: bool = True, normalize_to_s0: bool = True,
                          blur_mask: bool = True, return_complex: bool = False,
                          crop_to_box: Tuple[int, int, int, int] = None):
    """

    :param data_files:
    :param label_files:
    :param mask_files:
    :param n_averages:
    :param n_bvalues:
    :param mask_left_ventricle:
    :param normalize_to_s0:
    :param blur_mask:
    :param return_complex:
    :param crop_to_box:
    :return:
    """

    def _calc_md_fa(x, y):
        md = tf.reduce_mean(y[..., 1:4], axis=-1, keepdims=True)
        return x, md

    dataset_2d, masks = get_dataset_2d(data_files, label_files, mask_files, n_averages, n_bvalues, mask_left_ventricle,
                                       normalize_to_s0, blur_mask, return_complex, crop_to_box)
    return dataset_2d.map(_calc_md_fa), masks


if __name__ == '__main__':
    from glob import glob
    import os
    root = os.path.dirname(os.path.dirname(os.path.dirname(__file__))) + '/results/transfer/flat_training_data_0.0005'
    print(root)

    n_files = len(list(glob(f'{root}/mask_*.tfrecords')))
    data_filenames = [f'{root}/snr_inf/displaced_images_{i}.tfrecords' for i in range(n_files)]
    mask_filenames = [f'{root}/mask_{i}.tfrecords' for i in range(n_files)]
    labels_filenames = [f'{root}/LLSQ_ground_truth/tensors_{i}.tfrecords' for i in range(n_files)]
    print(data_filenames)

    # dataset, masks = get_scalar_dataset_2d(data_filenames, labels_filenames, mask_filenames, n_averages=4,
    #                                        n_bvalues=13, mask_left_ventricle=False, normalize_to_s0=True,
    #                                        blur_mask=True, crop_to_box=(80, 10, 70, 70))
    # for d, l in dataset.batch(3):
    #     print(d.shape, l.shape)

    dataset, masks = get_dataset_2d(data_filenames, labels_filenames, mask_filenames, n_averages=4, n_bvalues=13,
                                    mask_left_ventricle=False, normalize_to_s0=True, blur_mask=True,
                                    crop_to_box=(70, 0, 90, 86))

    for (d, l), m in tf.data.Dataset.zip((dataset, masks)).batch(3).take(1):
        print(d.shape, l.shape, m.shape)

    import sys
    sys.path.append(os.path.abspath("../../../cmrsim2/cmrsim"))
    import matplotlib.pyplot as plt
    import matplotlib
    matplotlib.use('TkAgg')
    from cmrsim.auxilliary.plot_utilities import SlideShow
    # sl = SlideShow(m[..., tf.newaxis].numpy())
    sl1 = SlideShow(np.abs(d.numpy()))
    # sl1 = SlideShow(np.abs(l[tf.newaxis, ...].numpy().transpose([3, 1, 2, 0])))
    # plt.show()
    sl1.fig.set_size_inches(12, 3)
    sl1.animate('inf_disp.gif')

    # dataset1d, masks = get_dataset_1d(data_filenames, labels_filenames, mask_filenames, n_averages=4, n_bvalues=13)
    # for (d, l), m in tf.data.Dataset.zip((dataset, masks)).batch(2).take(1):
    #     print(d.shape, l.shape, m.shape)