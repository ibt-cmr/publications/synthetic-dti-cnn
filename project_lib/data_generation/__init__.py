from project_lib.data_generation import map_interpolation as interpolation
# from project_lib.data_generation import sample_eigenvalues as sampling
# from project_lib.data_generation import full_pipeline as full_pipeline
# from project_lib.data_generation import myocard_tensormaps as myocard_maps
# from project_lib.data_generation import simulation as simulation


TISSUE_PROPERTIES = {
    "muscle": {"proton_density": 80, "T1": 1000, "T2": 50, "T2star": 35},
    "fat": {"proton_density": 70, "T1": 350, "T2": 30, "T2star": 20},
    "blood": {"proton_density": 95, "T1": 1500, "T2": 250, "T2star": 60},
    "liver": {"proton_density": 90, "T1": 800, "T2": 50, "T2star": 26},
    "bone": {"proton_density": 12, "T1": 250, "T2": 20, "T2star": 10}
}

MRXCAT_TO_TISSUE = {
    1: 'muscle',  # Myocard left ventricle
    2: 'muscle',  # Myocard right ventricle
    3: 'muscle',  # Myocard left atrium
    4: 'muscle',  # Myocard right atrium
    5: 'blood',  # Blood pool left ventricle
    6: 'blood',  # Blood pool right ventricle
    7: 'blood',  # Blood pool left atrium
    8: 'blood',  # Blood pool right atrium
    9: 'fat',  # Body Fat
    10: 'muscle',  # Non myocardium muscle
    13: 'liver',  # Liver
    31: 'bone',  # Ribs
    32: 'bone',  # Cortical bone
    33: 'bone',  # Spinal bone
    35: 'bone',  # Bone Marrow
    36: 'blood',  # Arteries
    37: 'blood',  # Vein
    50: 'fat'  # Pericardium
}

