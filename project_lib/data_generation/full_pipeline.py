""" Collection of convenience functions to run the pipeline of data generation
for Diffusion data"""

__all__ = ['load_motion_field', 'generate_tensormap', 'determine_label_mean_and_std', ]

from typing import Tuple, Iterable, Union, List
import os
import json
from glob import glob
from copy import deepcopy
from tqdm.notebook import tqdm

import numpy as np
import tensorflow as tf
from skimage.transform import resize
from skimage.morphology import erosion, disk

from cmrsim.anatomy.base import BaseDataset
import cmrsim.auxilliary.coordinates as cmrsim_coords

from project_lib.data_generation.myocard_tensormaps import get_interpolated_tensormap, get_lesion_map
from project_lib.fitting.dataset_utils import get_dataset_1d
from project_lib.comparison import clean_single_mask

from cdtipy.fitting import MatrixInversion
import cdtipy.utils


def generate_tensormap(n_lesions: int, mask, fov, samples_healthy, samples_lesion, seed_fraction: float = 1/10,
                       interpolation_kernel: str = 'rbf', e2a_kwargs: dict = None, kernel_kwargs: dict = None):
    """ Generates a left ventricle tensor-map including

    :param n_lesions: 
    :param mask:
    :param fov:
    :param samples_healthy:
    :param samples_lesion:
    :param seed_fraction:
    :param interpolation_kernel:
    :param e2a_kwargs:
    :param kernel_kwargs:
    :return:
    """
    ha_range = np.random.uniform(135, 155, 1)
    if n_lesions == 0:
        healthy_tensors = get_interpolated_tensormap(mask=mask, fov=fov, eigen_value_pool=samples_healthy,
                                                     seed_fraction=seed_fraction, ha_range=ha_range,
                                                     e2a_kwargs=e2a_kwargs, interpolation_kernel=interpolation_kernel,
                                                     kernel_kwargs=kernel_kwargs)
        return healthy_tensors, mask
    elif n_lesions > 0:
        lesion_map = get_lesion_map(mask, np.array(fov), n_lesions) # Fall back to default options
        healthy_tensors, helix_map, sheetlet_map = get_interpolated_tensormap(mask=mask, fov=fov, eigen_value_pool=samples_healthy,
                                                     seed_fraction=seed_fraction, ha_range=ha_range,
                                                     e2a_kwargs=e2a_kwargs, interpolation_kernel=interpolation_kernel,
                                                     kernel_kwargs=kernel_kwargs, return_angle_maps=True)
        
        lesion_tensors = get_interpolated_tensormap(mask=mask, fov=fov, eigen_value_pool=samples_lesion,
                                                    seed_fraction=seed_fraction, interpolation_kernel=interpolation_kernel,
                                                    kernel_kwargs=kernel_kwargs, helix_map=helix_map, sheetlet_map=sheetlet_map)

        mask_healthy = tf.where(lesion_map == 1., tf.ones_like(lesion_map), tf.zeros_like(lesion_map))
        mask_lesion = tf.where(lesion_map == 2., tf.ones_like(lesion_map), tf.zeros_like(lesion_map))
        masked_healthy_tensors = tf.einsum('xyij, xy -> xyij', healthy_tensors, mask_healthy)
        masked_lesion_tensors = tf.einsum('xyij, xy -> xyij', lesion_tensors, mask_lesion)
        return masked_lesion_tensors + masked_healthy_tensors, lesion_map
    else:
        raise ValueError(f"Invalid number of lesions: {n_lesions}")


def randomize_motion_fields(constant_displacements: np.ndarray):
    """ Shuffles and randomly scales repetitions of displacement fields.

    :param constant_displacements: (X, Y, 2, #b_vectors * #averages)
    :return:
    """
    n_reps = constant_displacements.shape[2]
    reps_first_displacements = constant_displacements.transpose([2, 0, 1, 3]).copy()
    np.random.shuffle(reps_first_displacements)
    random_displacements = reps_first_displacements.transpose([1, 2, 0, 3])

    scaling_factor = np.random.uniform(0.8, 1.2, size=(n_reps,))
    random_displacements = random_displacements * scaling_factor.reshape(1, 1, -1, 1)
    return random_displacements, np.squeeze(scaling_factor)


def load_motion_field(displacement_file: str, fov: Tuple[float, float], map_size: Tuple[int, int],
                      displacement_scale: float = 1e-4, randomize: bool = True):
    """ Loads motion field, calculates coordinates, according to field of view and map_size and randomizes
    stack of motion fields

    :param displacement_file: file that contains motion fields -> (X, Y, 2, #b_vectors, #averages)
    :param fov: (int, int)
    :param map_size: (int, int)
    :param displacement_scale: factor for scaling the pixel-wise displacements from registration motion field
    :param randomize
    :return:
    """
    displacements = np.load(displacement_file) # should have shape (X, Y, 2, N)
    
#     n_reps = np.prod(displacements.shape[-2:])
    n_reps = displacements.shape[-1]
    displacements = displacements.transpose([0, 1, 3, 2])
    displacements[..., 0] *= fov[0] / map_size[0] * displacement_scale
    displacements[..., 1] *= fov[1] / map_size[1] * displacement_scale
    flat_shape = displacements.shape[0:2] + (-1, 2)
    displacements = displacements.reshape(flat_shape)
    
    random_scaling = np.zeros(displacements.shape[-2])
    if randomize:
        displacements, random_scaling = randomize_motion_fields(displacements)
    
    # Set first repetition - displacement to zero to match the first average with GT
    displacements[:, :, 0, :] = 0.
    
    r = -1 * cmrsim_coords.get_static_2d_centered_coordinates(map_size, fov)
    static_coords = np.tile(r.numpy(), [1, 1, 1, 1, 1, 1])
    displaced_coordinates = np.tile(static_coords, [1, 1, 1, n_reps, 1, 1])
    displaced_coordinates[:, :, :, :, 0:2, :] += displacements[np.newaxis, ..., np.newaxis]
    return static_coords, displaced_coordinates, random_scaling


def run_simulation(data_dict: dict, simulators, static_coords: np.ndarray, displaced_coords: np.ndarray,
                   n_averages: int, object_res_mask: np.ndarray, snr_targets: Iterable[float], 
                   padding_factor: int = 1, voxel_batch_sizes=(10000, 3000), execute_eager: bool = False):
    """

    :param data_dict:
    :param simulators:
    :param n_averages:
    :param static_coords:
    :param displaced_coords:
    :param object_res_mask:
    :param snr_targets:
    :param padding_factor:
    :param voxel_batch_sizes:
    :param execute_eager: bool
    :return:
    """

    ref_simulator, disp_simulator = simulators

    print('Simulating static images... ')
    static_data_dict = deepcopy(data_dict)
    static_data_dict.update({'r_vectors': static_coords})

    simulated_k_space_ref = ref_simulator(BaseDataset(static_data_dict), voxel_batch_size=voxel_batch_sizes[0],
                                          execute_eager=execute_eager, unstack_repetitions=False)

    # Determine SNR
    mask = np.where(object_res_mask > 0, np.ones_like(object_res_mask), np.zeros_like(object_res_mask))
    snr_mask = resize(mask, ref_simulator.encoding_module.matrix_size.read_value().numpy().tolist())
    snr_mask = erosion(np.around(snr_mask), disk(1.75))
    noise_std = ref_simulator.determine_noise_level(simulated_k_space_ref, mask=snr_mask, target_snr=snr_targets)
    estimated_noise_levels = [0., *noise_std.tolist()]
    ref_simulator.encoding_module.absolute_noise_std.assign(tf.constant(estimated_noise_levels, dtype=tf.float32))
    
    # Add noise and Reconstruct, Loop to prevent OOM
#     print(f"\nReconstruction - Input shape: {simulated_k_space_ref.shape}")
    temp = []
    for i in range(n_averages):
        stacked_noisy_k_space = ref_simulator.encoding_module.add_noise(simulated_k_space_ref[..., 0, :])
        noisy_k_space = ref_simulator.forward_model.unstack_repetitions(stacked_noisy_k_space)
        temp += [ref_simulator.delayed_reconstruction(noisy_k_space).numpy(), ]
    static_images = np.stack(temp, axis=2)
    snr_map = ref_simulator.get_snr_map(noisy_k_space, 1)

    print('\nSimulating displaced images... ')
    displaced_data_dict = deepcopy(data_dict)
    displaced_data_dict.update({'r_vectors': displaced_coords})

    disp_simulator.encoding_module.absolute_noise_std.assign(tf.constant(estimated_noise_levels, dtype=tf.float32))
    simulated_k_space_data = disp_simulator(BaseDataset(displaced_data_dict), voxel_batch_size=voxel_batch_sizes[1],
                                            execute_eager=execute_eager)

#     print(f"\nReconstruction - Input shape: {simulated_k_space_data.shape}")
    # Add noise and Reconstruct, Loop to prevent OOM
    temp = []
    for i in range(disp_simulator.forward_model.dti_gauss.b_vectors.read_value().shape[0]):
        temp += [disp_simulator.delayed_reconstruction(simulated_k_space_data[..., i, :, :]).numpy(), ]
    temp = np.stack(temp, axis=1)

    displaced_images = np.reshape(temp, [temp.shape[0], -1, n_averages, *temp.shape[-3:]])
    return static_images, displaced_images, snr_map


def determine_label_mean_and_std(result_dir: str, index_list: List[int]):
    """ Calculates the mean and standard deviation over all ground truth label entries and writes them into the json
    file containing the simulation parameters
    :param result_dir:
    :param index_list: 
    :return:
    """

    with open(f"{result_dir}/parameters.json", "r") as paramfile:
        original_json = json.load(paramfile)
        parameters = original_json['example_offset_0']
        
    n_averages = parameters['n_averages']
    n_bvectors = len(parameters['b_vectors'])

    data_filenames = [f'{result_dir}/snr_inf/displaced_images_{i:04}.npz' for i in index_list]
    mask_filenames = [f'{result_dir}/labels/mask_{i:04}.npz' for i in index_list]
    labels_filenames = [f'{result_dir}/labels/label_{i:04}.npz' for i in index_list]
    dataset, masks = get_dataset_1d(data_filenames, labels_filenames, mask_filenames, n_averages=n_averages,
                                    n_bvalues=n_bvectors)

    # Calculate running mean and variance: https://stackoverflow.com/questions/56402955/whats-the-formula-for
    # -welfords-algorithm-for-variance-std-with-batch-updates
    mean, m2 = [tf.Variable(tf.zeros(8), dtype=tf.float32) for _ in range(2)]
    count = tf.Variable(0, dtype=tf.float32)
    for idx, (_, l) in tqdm(dataset.enumerate(), total=len(data_filenames)) :
        count.assign_add(l.shape[0])
        delta = l - mean.read_value()
        mean.assign_add(tf.reduce_sum(delta / count, axis=0))
        delta2 = l - mean.read_value()
        m2.assign_add(tf.reduce_sum(delta * delta2, axis=0))
        
    mu, sigma = mean.read_value(), tf.sqrt(m2.read_value() / (count.read_value() - 1))
    parameters.update({'labels_mean': mu.numpy().tolist(), 'labels_std': sigma.numpy().tolist()})
    original_json['example_offset_0'] = parameters
    with open(f'{result_dir}/parameters.json', 'w+') as hpfile:
        json.dump(original_json, hpfile)

        
def save_single_example(result_dir: str, index: int, static_images: tf.Tensor, displaced_images: tf.Tensor,
                        lesion_map: np.ndarray, b_vectors: np.ndarray, snr_targets: Iterable[Union[int, float]] = None):
    """

    :param result_dir: str
    :param index: int
    :param static_images: tf.Tensor (1, #b_vectors, #avg, N_noise, X, Y)
    :param displaced_images: tf.Tensor (1, #b_vectors, #avg, N_noise, X, Y)
    :param lesion_map: (X, Y)
    :param b_vectors: (#b_vectors, 3)
    :param snr_targets:
    :return:
    """

    if snr_targets is None:
        snr_targets = list(range(static_images.shape[-3]))

    os.makedirs(result_dir, exist_ok=True)
    binary_mask = (lesion_map > 0.).astype(np.int32)
    
    static_images = static_images[0].transpose([2, 3, 4, 0, 1])
    displaced_images = displaced_images[0].transpose([2, 3, 4, 0, 1])
    
    # Matrix inversion on noise-less and static data for ground truth
    os.makedirs(f'{result_dir}/labels', exist_ok=True)
    np.savez_compressed(f'{result_dir}/labels/mask_{index:04}', mask=lesion_map*np.array(binary_mask))

    for snr_idx, snr_val in enumerate(['inf', *[str(i) for i in snr_targets]]):
        os.makedirs(f'{result_dir}/snr_{snr_val}', exist_ok=True)
        np.savez_compressed(f'{result_dir}/snr_{snr_val}/static_images_{index:04}', image=static_images[snr_idx])
        np.savez_compressed(f'{result_dir}/snr_{snr_val}/displaced_images_{index:04}', image=displaced_images[snr_idx])

        
def determine_labels(result_dir: str, b_vectors: np.ndarray, index: int):
    """

    :param result_dir: str
    :param index: int
    :param static_images: tf.Tensor (1, #b_vectors, #avg, N_noise, X, Y)
    :param displaced_images: tf.Tensor (1, #b_vectors, #avg, N_noise, X, Y)
    :param lesion_map: (X, Y)
    :param b_vectors: (#b_vectors, 3)
    :param snr_targets:
    :return:
    """
    
    binary_mask = np.load(f"{result_dir}/labels/mask_{index:04}.npz")["mask"]
    static_noiseless_images = np.load(f"{result_dir}/snr_inf/static_images_{index:04}.npz")["image"][..., 0:1]

    r = MatrixInversion(static_noiseless_images, b_vectors, binary_mask)
    r.fit(disable_progressbar=True)
    llsq_2d_result = cdtipy.utils.fill_dense_map(binary_mask, tf.cast(tf.concat(r.get_result(), axis=1), tf.float32))
        
    binary_mask, clean_tensors, fa, md = clean_single_mask(np.array(binary_mask, np.float32), 
                                                           np.array(llsq_2d_result[ ..., 1:], np.float32),
                                                           thresholds=(8, 12.5))
    label = np.concatenate([clean_tensors, fa[..., np.newaxis], md[..., np.newaxis]], axis=-1)
    
    np.savez_compressed(f'{result_dir}/labels/clean_mask_{index:04}', mask=np.array(binary_mask))
    np.savez_compressed(f'{result_dir}/labels/label_{index:04}', label=label)
    