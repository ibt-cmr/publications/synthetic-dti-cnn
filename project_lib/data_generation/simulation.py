__all__ = ['DiffusionSimulation', 'simulate_static_images', 'simulate_displaced_images']
from typing import Union, List

import tensorflow as tf
import numpy as np
from skimage.transform import resize
from skimage.morphology import erosion, disk

from cmrsim.anatomy.base import BaseDataset
from cmrsim.signal_processes.base import CompositeSignalModel
import cmrsim.signal_processes.solution_operators as solution_operators
from cmrsim.reconstruction.cartesian import Cartesian2D, RoemerMultiCoil
from cmrsim.encoding.epi import EPI
import cmrsim.simulation as cmr_simulate
import cmrsim.auxilliary.snr as cmrsim_snr


class DiffusionSimulation(cmr_simulate.SimulationBase):

    def _build(self):  # noqa
        # Encoding definition
        encoding_module = EPI(field_of_view=(0.31, 0.112), sampling_matrix_size=(121, 43), bandwidth_per_pixel=1300.,
                              blip_duration=0., k_space_segments=43, acquisition_start=-25.7692,
                              absolute_noise_std=0.)

        # Signal module construction
        diffusion_module = solution_operators.diffusion_weighting.ConstantGaussianDiffusion(
            b_vectors=tf.zeros(shape=(1, 3), dtype=tf.float32))
        diffusion_module.expansion_factor = 1
        coil_sens_module = solution_operators.coil_sensitivities.CoilSensitivity(
            tf.zeros((4, 1, 1, 1), dtype=tf.complex64), (0.3024, 0.1072, 0.))
        t2_star_module = solution_operators.t2_star.StaticT2starDecay(encoding_module.get_sampling_times())

        # Forward model composition
        forward_model = CompositeSignalModel(diffusion_module, coil_sens_module, t2_star_module)
        return forward_model, encoding_module, None
    
    
    def update_cs(self, padding_factor: int = 2):
        coil_sens_maps = self.forward_model.coil_sensitivities.look_up_map3d.read_value()
        sampling_matrix_size = self.encoding_module.matrix_size.read_value()
        
        if padding_factor is not None and padding_factor > 0:
            resize_shape = (coil_sens_maps.shape[0], *(sampling_matrix_size * padding_factor), coil_sens_maps.shape[-1])
            padding = (padding_factor - 1) * tf.cast(sampling_matrix_size, tf.float32)
            padding = [(int(tf.math.floor(c / 2)), int(tf.math.ceil(c / 2))) for c in padding]
        else:
            resize_shape = (coil_sens_maps.shape[0], *sampling_matrix_size, coil_sens_maps.shape[-1])
            padding = None
        coil_sens_maps_resized = tf.complex(resize(tf.math.real(coil_sens_maps).numpy(), resize_shape),
                                            resize(tf.math.imag(coil_sens_maps).numpy(), resize_shape))

        self._img_res_coil_sens_maps = coil_sens_maps_resized
        self._delayed_recon_module = RoemerMultiCoil(sample_matrix_size=sampling_matrix_size,
                                                     padding=padding, coil_sensitivities=coil_sens_maps_resized[..., 0])
            
    
    def determine_noise_level(self, simulated_k_space: tf.Tensor, mask: Union[np.ndarray, tf.Tensor],
                              target_snr: np.ndarray) -> tf.Tensor:
        sampling_matrix_size = self.encoding_module.matrix_size.read_value()
        channel_wise_recon = Cartesian2D(sample_matrix_size=sampling_matrix_size)
        single_coil_images = channel_wise_recon(simulated_k_space)
        unstacked_channelwise_images = self.forward_model.unstack_repetitions(single_coil_images)
        
        with tf.device("CPU:0"):
            noise_std_deviations = cmrsim_snr.compute_noise_std(
                                        noiseless_single_coil_images=unstacked_channelwise_images[0, :, 0, 0, ...],
                                        coil_sensitivities=self._img_res_coil_sens_maps[..., 0],
                                        target_snr=target_snr,
                                        mask=mask,
                                        iteratively_refine=False)
        return noise_std_deviations
    
    @tf.function
    def delayed_reconstruction(self, simulated_k_space: tf.Tensor):
        n_channels = tf.shape(self.forward_model.coil_sensitivities.look_up_map3d)[0]
        reconstruction = self._delayed_recon_module(simulated_k_space, coil_channel_axis=1)
        return reconstruction / tf.cast(n_channels, tf.complex64)
    
    def get_snr_map(self, simulated_k_space: tf.Tensor, repetition_index: int = 0, noise_index: int = 1):
        sampling_matrix_size = self.encoding_module.matrix_size.read_value()
        coil_sens_maps = self.forward_model.coil_sensitivities.look_up_map3d.read_value()
        n_channels = coil_sens_maps.shape[0]
        resize_shape = (n_channels, *sampling_matrix_size, coil_sens_maps.shape[-1])
        coil_sens_maps_resized = tf.complex(resize(tf.math.real(coil_sens_maps).numpy(), resize_shape),
                                            resize(tf.math.imag(coil_sens_maps).numpy(), resize_shape))
        
        if n_channels == 1:
            simulated_k_space = simulated_k_space[:, tf.newaxis, ...]
        
        channel_wise_recon = Cartesian2D(sample_matrix_size=sampling_matrix_size)
        single_coil_images = channel_wise_recon(simulated_k_space)
        lowest_snr = self.encoding_module.absolute_noise_std[noise_index]
        a = single_coil_images[0, :, repetition_index, noise_index, :, :]
        dynamic_noise_scan = tf.complex(tf.random.normal(shape=a.shape, mean=0, stddev=lowest_snr),
                                        tf.random.normal(shape=a.shape, mean=0, stddev=lowest_snr))
        snr_map = cmrsim_snr.calculate_snr(a, dynamic_noise_scan, coil_sens_maps_resized[..., 0])
        return snr_map

    
def set_up_simulators(b_vectors, n_averages, fov, acquisition_matrix, coil_sensitivities):
    static_simulator = DiffusionSimulation(name='static_simulator')
    static_simulator.encoding_module.fov.assign(fov)
    static_simulator.encoding_module.matrix_size.assign(acquisition_matrix)
    static_simulator.forward_model.coil_sensitivities.look_up_map3d.assign(coil_sensitivities)
    static_simulator.forward_model.dti_gauss.b_vectors.assign(np.array(b_vectors, np.float32))

    dynamic_simulator = DiffusionSimulation(name='displaced_simulator')
    dynamic_simulator.encoding_module.fov.assign(fov)
    dynamic_simulator.encoding_module.matrix_size.assign(acquisition_matrix)
    dynamic_simulator.forward_model.coil_sensitivities.look_up_map3d.assign(coil_sensitivities)
    dynamic_simulator.forward_model.dti_gauss.b_vectors.assign(tf.repeat(b_vectors, axis=0, repeats=n_averages))
    return static_simulator, dynamic_simulator

