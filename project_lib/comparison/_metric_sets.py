""" Module to process a batch of inferred tensors and compare several inference methods"""
from typing import List, Tuple, Iterable

import tensorflow as tf
import numpy as np
import tqdm.notebook
from tqdm.notebook import tqdm

import cdtipy


def list_of_analysis(ground_truth: List[tf.Tensor], predictions: List[tf.Tensor], mask: tf.Tensor, fov: tf.Tensor, error_measures: List[callable, ]):
    """
    
    :return: List[List[dict]], List[List[dict]]
             Outer list lengths are #predictions+1/#predictions
             Inner list lengths are #Examples/#Examples
             Dictionaries contain full cdtipy metrics/angulation analysis per example
    """
    
    errors_per_prediction = [[[] for _ in range(len(predictions))] for em in error_measures]
    inferences_per_prediction = [[] for _ in range(len(predictions) + 1)]
    
    with tqdm(total=len(ground_truth), desc="Example: ") as progress_bar, \
            tqdm(total=len(predictions), desc="Prediction: ") as progress_bar2:
        for example_index, g, m in zip(range(len(ground_truth)), ground_truth, mask):
            
            nan_mask = m.copy()
            nan_mask[nan_mask < 0.5] = np.nan
            
            gt_metrics = cdtipy.tensor_metrics.metric_set(g, flat_tensors=True)
            gt_angulation = cdtipy.angulation.angulation_set(eigen_vectors=gt_metrics['evecs'], mask=m, field_of_view=fov)
            gt_metrics = {k: np.einsum('xy..., xy -> xy...', v, nan_mask) for k,v in gt_metrics.items()}
            gt_results = {**gt_metrics, **gt_angulation}
            inferences_per_prediction[0].append(gt_results)
    
            progress_bar2.reset()
            for prediction_index, pred in enumerate(predictions): 
                p = pred[example_index]
                p_metrics = cdtipy.tensor_metrics.metric_set(p, flat_tensors=True)
                p_angulation = cdtipy.angulation.angulation_set(eigen_vectors=p_metrics['evecs'], mask=m, field_of_view=fov)
                p_metrics = {k: np.einsum('xy..., xy -> xy...', v, nan_mask) for k,v in p_metrics.items()}
                
                p_results = {**p_metrics, **p_angulation}
                inferences_per_prediction[prediction_index + 1].append(p_results)
                
                for err_idx, error_measure in enumerate(error_measures):
                    p_diff = {k: error_measure(gt_results[k], v_p) for k, v_p in p_results.items()}
                    errors_per_prediction[err_idx][prediction_index].append(p_diff)

                progress_bar2.update(1)
            progress_bar.update(1)
    return inferences_per_prediction, errors_per_prediction


def flatten_results(results: List[List[dict]], mask_per_example: Iterable[np.ndarray]):
    """ 
    :param results: List[List[dict]]
    :param mask_per_example: Iterable[np.ndarray] len must match length of the inner List in argument results
    """
    flat_result_per_model = []
    for list_of_examples in results:
        result_keys = list_of_examples[0].keys()
        flat_results = {k: [] for k in result_keys}
        for result_dict, example_mask in zip(list_of_examples, mask_per_example):
            mask_indices = np.where(example_mask > 0.1)
            for k, v in result_dict.items():
                flat_results[k].append(v[mask_indices])
        flat_result_per_model.append({k: np.concatenate(v, axis=0) for k, v in flat_results.items()})
    
    return flat_result_per_model


def print_mean(errs: dict):
    """ Assumes errs to be a dictionary that contains 1. sequence of different results, 2. Tensormetrics 3. Ragged tensors of metrics per example
    """
    for pred_k, pred_dict in errs.items():
        print('\n--------------', f"Prediction: {pred_k}", '--------------')  
        for k, v in pred_dict.items():
            v = v.flat_values
            try:
                print(f'{k}:  ({tf.reduce_mean(v, axis=(0, )).numpy(): 1.5f} +- {tf.math.reduce_std(v, axis=(0, )).numpy(): 1.6f})')
            except TypeError:
                print(f'{k}:  ({np.round(tf.reduce_mean(v, axis=(0, )), decimals=5)} +- {np.round(tf.math.reduce_std(v, axis=(0, )), decimals=6)})')

                
def print_median(errs: dict):
    for pred_k, pred_dict in errs.items():
        print('\n--------------', f"Prediction: {pred_k}", '--------------')  
        for k, v in pred_dict.items():
            try:
                print(f'{k}:  ({np.median(v,axis=(0, 1)): 1.5f} +-' +
                      f'{np.percentile(v, q=90, axis=(0, 1)): 1.6f})')
            except TypeError:
                print(f'{k}:  ({np.round(np.percentile(v, q=50, axis=(0, 1)), decimals=5)} +-' 
                      + f' {np.round(np.percentile(v, q=90, axis=(0, 1)), decimals=6)})')