__all__ = ["non_outlier_mask", "clean_masks", "clean_single_mask"]

import numpy as np
from tqdm.notebook import tqdm
import cdtipy

def non_outlier_mask(data, multiple_threshold=20):
    """ Cleans the given data from outliers that are larger than m times the median of 
    all absolute difference of all data points to the median data
    :returns:
    """
    abs_distances_to_median = np.abs(data - np.median(data))
    median_distance = np.median(abs_distances_to_median)
    multiple_of_median_distance = abs_distances_to_median / median_distance if median_distance else 0. 
    outlier_mask = multiple_of_median_distance < multiple_threshold
    return outlier_mask


def clean_masks(stacked_masks, tensor_maps, thresholds=(10, 15)):
    """ Cleans mask by checking if any pixels within the mask contain eigevalues higher than 3e-3 and rejects outliers 
    in MD and FA
    """
    
    clean_masks = []
    clean_tensor_maps = []
    for idx, m in tqdm(enumerate(stacked_masks.copy()), total=stacked_masks.shape[0]):
        t = cdtipy.utils.flat_tensor_to_matrix(tensor_maps[idx])
        evals, evecs = cdtipy.tensor_metrics.eigen_decomposition(t)
        md = cdtipy.tensor_metrics.mean_diffusivity(t)
        fa = cdtipy.tensor_metrics.fractional_anisotropy(evals)
        ref_outlier = np.logical_and(non_outlier_mask(md[m > 0.5], thresholds[0]), non_outlier_mask(fa[m > 0.5], thresholds[1]))
        ref_outlier = np.logical_and(np.all(evals < 2.9e-3, axis=-1)[m > 0.5], ref_outlier).astype(np.float64)
        m[m>0.5] = ref_outlier.astype(np.float64)
        clean_masks.append(m)
        clean_tensor_maps.append(cdtipy.utils.flatten_tensor(np.where(m[..., np.newaxis, np.newaxis] > 0, t, np.zeros_like(t))))
    return np.stack(clean_masks, axis=0)


def clean_single_mask(mask, tensor_map, thresholds=(10, 15)) -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray):
    """ Cleans mask by checking if any pixels within the mask contain eigevalues higher than 3e-3 and rejects outliers 
    in MD and FA
    :return: cleaned: (mask, tensors, fa, md )
    """
    
    t = cdtipy.utils.flat_tensor_to_matrix(tensor_map)
    evals, evecs = cdtipy.tensor_metrics.eigen_decomposition(t)
    md = cdtipy.tensor_metrics.mean_diffusivity(t)
    fa = cdtipy.tensor_metrics.fractional_anisotropy(evals)
    
    ref_outlier = np.logical_and(non_outlier_mask(md[mask > 0.5], thresholds[0]), non_outlier_mask(fa[mask > 0.5], thresholds[1]))
    ref_outlier = np.logical_and(np.all(evals < 2.9e-3, axis=-1)[mask > 0.5], ref_outlier).astype(np.float64)
    return_mask = mask.copy()
    return_mask[mask>0.5] = ref_outlier.astype(np.float64)
    tensor_map = np.einsum("xy, xy... -> xy...", return_mask, tensor_map)
    return (return_mask, tensor_map, fa*return_mask, md*return_mask)