__all__ = ["load_model", "load_artificial_dataset", "check_for_models_in_dir"]

import json
from glob import glob
import os 

import tensorflow as tf
import numpy as np

import project_lib.fitting.dataset_utils as dataset_utils


def load_model(directory: str, model_class: tf.keras.Model, mean_scale=1., std_scale=1., **kwargs):
    with open(directory + '/hp.json', 'r') as hp_file:
        hp_dict = json.load(hp_file)
    input_shape = tuple(hp_dict['input_shape'])
    del hp_dict['input_shape']
    new_model = model_class(out_mean=tf.constant(hp_dict['output_mean'], dtype=tf.float32, shape=(8, )) * mean_scale,
                            out_std=tf.constant(hp_dict['output_std'], dtype=tf.float32, shape=(8, )) * std_scale,
                            input_shape=input_shape, **hp_dict)
    new_model.load_weights(directory+ '/checkpoints/intermediate_weights.ckp')
    return new_model, hp_dict


def load_artificial_dataset(resource_dir: str, motion: str, snr: str, shuffle_seed=None,  n_averages: int = 4, number_files: int = None, 
                            dataset_kwargs: dict = dict(mask_left_ventricle=True, normalize_to_s0=True, blur_mask=True,
                            return_complex=False, mask_dilation=3), **kwargs):
    
    data_samples = glob(f"{resource_dir}/labels/clean_mask_*.npz")
    index_list = [int(os.path.basename(f)[-8:-4]) for f in data_samples]
    index_list.sort()
    
    
    print(f"Loading dataset with: Motion = {motion} | SNR = {snr}")
    if number_files is None:
        number_files = len(list(glob(f'{resource_dir}/snr_{snr}/{motion}_images_*')))
    
    data_filenames = [f'{resource_dir}/snr_{snr}/{motion}_images_{i:04}.npz' for i in index_list[:number_files]]
    labels_filenames = [f'{resource_dir}/labels/label_{i:04}.npz' for i in index_list[:number_files]]
    mask_filenames = [f'{resource_dir}/labels/clean_mask_{i:04}.npz' for i in index_list[:number_files]]

    with open(f'{resource_dir}/parameters.json', 'r+') as file:
        h = json.load(file)['example_offset_0']
        b_vectors = np.array(h['b_vectors'])
        fov = h['fov']
    n_bvalues = b_vectors.shape[0]
        
    data, dilated_masks = dataset_utils.get_dataset_2d(data_filenames, labels_filenames, mask_filenames,
                                                       n_averages, n_bvalues, **dataset_kwargs)

    mask_filenames = [f'{resource_dir}/labels/mask_{i:04}.npz' for i in index_list[:number_files]]
    original_masks = np.stack([np.load(f)["mask"] for f in mask_filenames], axis=0)
    original_masks = tf.data.Dataset.from_tensor_slices(original_masks)
    return data.batch(1), dilated_masks.batch(1), b_vectors, fov, original_masks


def check_for_models_in_dir(basedir: str, modelname: str = 'multi_resnet2d'):
    """ Recursively searches for saved models inside the specified basedir where the given modelname
    needs to be incorporated in the directory name for a single saved instance
    """

    trained_dirs = glob(f'{basedir}/**/{modelname}*', recursive=True)
    model_dict = {}
    
    for model_dir in trained_dirs:
        if os.path.isfile(f'{model_dir}/hp.json') and os.path.isfile(f'{model_dir}/checkpoints/checkpoint'):
            with open(f'{model_dir}/hp.json', 'r+') as _:
                hp_dict = json.load(_)
                motion, snr = hp_dict['motion'], hp_dict['snr']
                if f'{motion[0]}_{snr}' in model_dict.keys():
                    model_dict[f'{motion[0]}_{snr}'].append(f"{os.path.basename(model_dir)} : {os.path.basename(os.path.dirname(model_dir.replace(basedir, ' ')))} :")
                else:
                    model_dict[f'{motion[0]}_{snr}'] = [f"{os.path.basename(model_dir)} : {os.path.basename(os.path.dirname(model_dir.replace(basedir, ' ')))} :"]
    return model_dict

