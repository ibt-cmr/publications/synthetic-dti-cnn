__all__ = ["plot_histograms", "plot_maps", "plot_maps", "plot_scatter_r2", "plot_box"]

from typing import List, Tuple

from matplotlib import gridspec
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import math
import seaborn as sns

import sys
sys.path.append('../../cdtipy')
import cdtipy


def plot_histograms(flat_data: dict, names: List[str], bins: List[np.ndarray], 
                    xranges: List[Tuple[float, float]], legend_names: List[str],
                    hist_kwargs: Tuple[dict], fontsize: float = 8., filter_function: callable = None,
                    ylim: float = 0.1, f_axes = None, legend=True, text=True):
    """ Creates a figure with 5 subplots containing the histograms of (MD, FA, EV1, EV2, EV3) for each entry of the 
    argument list of dictionaries flat_data. 
    
    :param flat_data: List[dict] each dictionary must contain the keys (MD, FA, evals) with values of shape (None, ), (None, ), (None, 3)
    :param bins: List of arrays defining the bins used for ax.hist  
    :param xranges: x lims of the 5 subplots
    :param legend_names: List[str]  with length equal to flat_data / names of models 
    :param hist_kwargs:  List[dict] kwargs per entry in flat_data, passed to ax.hist function to style the plot per model
    :param filter_function: callable -> is called on the data if not None. Can be used to filer out certain data points
    :param ylim: float upper ylim
    :returns: fig, axes
    """ 
    if f_axes is None:
        f, axes = plt.subplots(1, 5, sharey=True)
    else: 
        f, axes = f_axes
    
    [a.set_ylim([0, ylim]) for a in axes]

    for model_index, data, c in zip(range(len(flat_data)), flat_data, [f"C{i}" for i in range(10)]):
        plot_prepared_data = [data[i] for i in ['MD', 'FA']] + [data['evals'][..., i] for i in range(3)]
        density_checklist = []
        means = []
        for xr, b, n, a, plot_values in zip(xranges, bins, names, axes, plot_prepared_data):
            if filter_function is not None:
                 plot_values = filter_function(plot_values)
            means.append(plot_values.mean()), density_checklist.append(np.around(plot_values.sum(), decimals=5))
            weights = np.ones_like(plot_values) / plot_values.shape[0]
            
            if hist_kwargs[model_index].get("color", None) is None:
                hist_kwargs[model_index]["color"] = c
            sns.histplot(plot_values, stat="probability", bins=b, ax=a, **hist_kwargs[model_index])
            
            if text:
                text_anchor_coords = [xr[-1] - (xr[-1] - xr[0]) * 0.4, a.get_ylim()[-1]*(0.965 - model_index * 0.04)]
                text_string = f'$\mu\pm\sigma$ {plot_values.mean() * 10.**(-math.floor(math.log10(xr[-1]))): 1.2f}' \
                              + f'$\pm${plot_values.std() * 10.**(-math.floor(math.log10(xr[-1]))): 1.2f}'
                a.text(*text_anchor_coords, text_string, fontsize=fontsize)
            a.set_xticks(np.linspace(xr[0], xr[-1], 6)), a.ticklabel_format(axis='both', style='sci', scilimits=(0, 0)), a.set_xlim(xr)
            a.set_xlabel(f"{n}", fontsize=fontsize+4, fontweight='normal'), a.grid(True)
    
    handles = [patches.Patch(facecolor=f"C{i}") for i in range(len(legend_names))] 
    if legend:
        axes[0].legend(handles=handles, labels=legend_names, loc='upper left', fontsize=fontsize, frameon=False)
    
    [() for a, xr in zip(axes, xranges)]
    f.set_size_inches((18, 5.)), f.tight_layout()#, f.subplots_adjust(right=0.96, left=0.03, top=0.93, bottom=0.15, wspace=0.25)
    return f, axes



def plot_maps(data: List[dict], metric_name: str, titles: List[str], suptitle: str,
               cmap: str = None, ref_scale: int = 0, vmax_factor=1):
    """ Creates a figure with one subplot in which the concatenated maps of the metric specified by parameter metric_name
    is displayed. Assumes that all the values in the data dict have the same shape.
    
    :param data:  List[dict] each dictionary must contain the keys (MD, FA, HA, E2A) with values of shape (X, Y)
    :param data: metric name defines the entry which is plotted   
    """
    
    f, a = plt.subplots(1, 1)
    a.set_xticks([]), a.set_yticks([])
    
    temp = data[ref_scale][0][metric_name]
    vmin, vmax = np.min(temp[np.isfinite(temp)]), np.max(temp[np.isfinite(temp)]) * vmax_factor
    
    concatenated_arr = np.concatenate([d[0][metric_name] for d in data], axis=1)
    image_artist = a.imshow(concatenated_arr, vmin=vmin, vmax=vmax, cmap=cmap)
    divider = make_axes_locatable(a)
    cax = divider.append_axes('right', size='1%', pad=0.05)
    cbar = plt.colorbar(image_artist, cax=cax)
    cbar.ax.ticklabel_format(style='sci', scilimits=(0, 0))
    
    f.set_size_inches(3.*len(data), 3.5), f.subplots_adjust(left=0.05, right=0.9, bottom=0.05, top=0.95)
    [a.text(a.get_xlim()[1] * ((2*i+1)/(2*len(titles))), a.get_ylim()[0] + 5, t, ha='center', va='center') for i, t in enumerate(titles)] 
    f.suptitle(suptitle, fontweight="bold", fontsize=20)
    
    return f, a, image_artist


def plot_scatter_r2(ref: List[np.ndarray], model_inf: List[np.ndarray], r2_method: callable):
    """
    :param ref: List of the flattened arrays for MD, FA, E2A, EV1, EV2, EV3
    
    """
    f, a = plt.subplots(2, 3)
    a_flat = [ax for ax_row in a for ax in ax_row]
    r2_score_list = []
    plot_titles = ["MD", "FA", "E2A", "EV1", "EV2", "EV3"]
    
    for i in range(len(plot_titles)+1):
        clean_ref, clean_inf = ref[i], model_inf[i]

        finite_idx = np.where(np.logical_and(np.isfinite(clean_ref), np.isfinite(clean_inf))) 

        r2 = r2_method(clean_ref[finite_idx], clean_inf[finite_idx])
        r2_score_list.append(r2)
        
        if i < 6:
            a_flat[i].set_title(plot_titles[i], fontweight='bold', fontsize=8)
            a_flat[i].set_xlabel("ground truth"), a_flat[i].set_ylabel("Inference"), a_flat[i].grid(True)
            a_flat[i].ticklabel_format(axis='both', style='', scilimits=(0, 0))

            a_flat[i].scatter(clean_ref, clean_inf, s=0.5, marker='x')
            lims = [*a_flat[i].get_ylim(), *a_flat[i].get_xlim()]
            lo,up = np.min(lims), np.max(lims)
            a_flat[i].plot([lo, up], [lo, up], '-', color='r')
            a_flat[i].set_xticks(a_flat[i].get_yticks())
            a_flat[i].set_yticks(a_flat[i].get_yticks())
            a_flat[i].set_xlim([lo, up]), a_flat[i].set_ylim([lo, up])         
            a_flat[i].text(0.02, 0.95, f"$R^2$: {r2: 1.4f}", horizontalalignment='left',
                           verticalalignment='top', transform = a_flat[i].transAxes)
    return f, a, r2_score_list

def plot_box(flat_signed_errors, model_names, fig_size=(12, 8), whis=3, faxes=None, 
             plot_titles=("MD", "FA", "E2A", "EV1", "EV2", "EV3"), 
             unit_strings=("$mm^2/s$", "", "$\degree$", "$mm^2/s$", "$mm^2/s$", "$mm^2/s$")):
    """
    :param flat_signed_errors: List of dictionaries containing the signed errors per 'model'
    """
    if faxes is None:
        f, axes = plt.subplots(2, 3)
    else:
        f, axes = faxes
    
    axes = axes.flatten()                   

#     errs_per_model = [[flat_signed_errors[model_index][k] for k in plot_titles[0:-3]] + 
#                       [flat_signed_errors[model_index]['evals'][..., i] for i in range(3)]
#                       for model_index in range(len(model_names))]

    errs_per_model = [[flat_signed_errors[model_index][k] for k in plot_titles] for model_index in range(len(model_names))]

    percentiles = []
    for i, ax, title in zip(range(len(axes)), axes, plot_titles):
        data = np.array([err[i] for err in errs_per_model]).T
        percentiles.append(np.nanpercentile(data, [25, 50, 75], axis=0))
        sns.boxplot(data=data, ax=ax, whis=whis, fliersize=0.5, saturation=0.9, linewidth=1.)


    for a, title, unit in zip(axes, plot_titles, unit_strings):
        a.ticklabel_format(axis='y', style='', scilimits=(-2, 3))
        a.set_xlim([-0.75, len(flat_signed_errors)-0.25])
        a.set_xticks([i for i in range(0, len(model_names))])
        a.set_xticklabels(model_names, fontdict={'fontsize':10}, minor=False, rotation=45)    
        a.grid(True, linestyle="--", alpha=0.6)
        a.set_title(title)
        a.set_ylabel(unit)
    f.suptitle("Signed Errors")
    f.set_size_inches(fig_size)
    f.tight_layout()
    return f, percentiles



def plot_maps_histogram_combo(all_results: List[List[dict]], mask, fig_axes=None, data=None, xy_crop=(40,150),
                              legend_names=("$LLSQ_{Reg}$", "Network", "$LLSQ_{Unreg}$")):
    
    if fig_axes is None:
        outer = gridspec.GridSpec(2, 1, height_ratios = [4, 2]) 
        gs1 = gridspec.GridSpecFromSubplotSpec(len(all_results)+1, 4, subplot_spec = outer[0], hspace=-0.1, wspace=0.01, height_ratios=[0.98/3, ] * len(all_results) + [0.02,])
        gs2 = gridspec.GridSpecFromSubplotSpec(2, 4, subplot_spec = outer[1], hspace =.05, wspace=0.15, height_ratios=[0.15, 0.85])

        fig = plt.figure(figsize=(16, 8))

        axes = []
        for i in range(len(all_results)):
            for j in range(4):
                ax = fig.add_subplot(gs1[i, j])
                ax.axis("off")
                axes.append(ax)
        axes = np.array(axes).reshape(-1, 4)

        caxes = [fig.add_subplot(gs1[-1, i]) for i in range(4)]

        hist_axes = []
        for j in range(4):
            _ = fig.add_subplot(gs2[1, j])
            hist_axes.append(_)
        hist_axes = np.array(hist_axes)

        box_axes = []
        for j in range(4):
            _ = fig.add_subplot(gs2[0, j])
            box_axes.append(_)
        box_axes = np.array(box_axes)   
        
        dumy_fig, dummy_ax = plt.subplots(1, 1)
        
    else:
        fig, (axes, caxes, box_axes, hist_axes, dummy_ax) = fig_axes
       
    
    x_ranges = [(0.5e-3, 2.5e-3), (0.0, 0.65), (-92, 92), (-5, 95)]
    bins = [np.linspace(*i, 50) for i in x_ranges]        
    hist_kwargs = [dict(color="C0", bins=bins, vert_line_color=None, hist_kw=dict(stat="probability", kde=True, fill=False, linewidth=0, element="bars")), 
                   dict(color="C2", bins=bins, vert_line_color=None, box_position=-0.5, hist_kw=dict(stat="probability", kde=True, fill=False, linewidth=0, element="bars", line_kws=dict(linestyle="-"))),
                   dict(color="C1", bins=bins, vert_line_color=None, box_position=-1., hist_kw=dict(stat="probability", kde=True, fill=False, linewidth=0, element="bars", line_kws=dict(linestyle="--"))),
                  ]
    
    for row_idx, results in enumerate(all_results):
        cdtipy.plotting.scalars.imshow_metrics({k: v[::-1][xy_crop[0]:xy_crop[1],xy_crop[0]:xy_crop[1]].T for k, v in results.items()},
                                               data[::-1].transpose(1,0,2)[xy_crop[0]:xy_crop[1], xy_crop[0]:xy_crop[1]], 
                                               (fig, [*axes[row_idx, 0:2], dummy_ax]), colorbars=False)
        cdtipy.plotting.scalars.imshow_angles({k: v[::-1][xy_crop[0]:xy_crop[1],xy_crop[0]:xy_crop[1]].T for k, v in results.items()},
                                               data[::-1].transpose(1,0,2)[xy_crop[0]:xy_crop[1],xy_crop[0]:xy_crop[1]],
                                              (fig, [*axes[row_idx, 2:4], dummy_ax]), colorbars=False)
        cdtipy.plotting.scalars.hist_summary(results, results, mask=mask, plot_eigenvals=False, f_axes=(fig, (box_axes, hist_axes)), **hist_kwargs[row_idx])

    [_.set_title("") for _ in [*axes[1:].flatten(), *box_axes]]
    [(_.set_xticklabels([]), _.grid(True)) for _ in box_axes]
    [_.set_xlim(*r) for _, r in zip(box_axes, x_ranges)]
    [_.set_xlim(*r) for _, r in zip(hist_axes, x_ranges)]
    [(_.set_ylabel(""), _.set_yticklabels([])) for _ in hist_axes]

    for i, ax, t in zip(range(4), axes[-1], ["mm$^2$/s", "a.u.", "°", "°"]):       
        cbar = fig.colorbar(ax.get_children()[-2], cax=caxes[i], ax=ax, orientation='horizontal', shrink=0.1, pad=0.05)
        cbar.formatter.set_powerlimits((-2, 2))
    [_.text(0.02, 0.95, t, horizontalalignment='left', verticalalignment='top', transform=_.transAxes, fontdict=dict(color="white", size=14)) 
     for _, t in zip(axes[:, 0], legend_names)]

    [_.legend(legend_names, fontsize=10) for _ in hist_axes]
    
    return fig, (axes, caxes, box_axes, hist_axes, dummy_ax)