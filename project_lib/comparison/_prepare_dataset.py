from typing import Tuple

import tqdm.notebook
from tqdm.notebook import tqdm

import tensorflow as tf
import numpy as np

import cdtipy

    
def infer_comparison(dataset: tf.data.Dataset, masks: tf.data.Dataset,
                     models: Tuple[tf.keras.Model, ], b_vectors: np.ndarray, n_examples: int = 100):
    """ Calls predict method of all models in input argument, saves ground truth and LSQ.
    Expected dataset output shape: 1D: n_pixels, ..., 2D: (1, X, Y, ...) 
    
    :param dataset: batched_dataset
    :param masks: batched dataset
    :param models: List of load models
    :param b_vectors: np.ndarray of shape (None, 3)
    :param n_examples: number of examples in dataset just to display progress bar
    :return: List[np.ndarray], List[List[np.ndarray]], List[np.ndarray], List[np.ndarray]
            ground_truth, model_predictions, MatrixInversion, Masks
    """
    
    predictions = [[] for _ in models]
    gt, lsq, masks_ = [], [], []
    
    with tqdm(total=n_examples, desc="Example: ") as progress_bar:
        for idx, ((data, labels), mask) in tf.data.Dataset.zip((dataset, masks)).enumerate():
            mask_indices = tf.where(mask > 0)
            gt += [tf.squeeze(labels[...]).numpy(), ]
            mask = tf.squeeze(mask)
            masks_.append(mask.numpy())

            for model_index, m in enumerate(models):
                p = m.predict(data)
                predictions[model_index].append(np.array(p[0]))

            data = tf.reshape(data, [*mask.shape, b_vectors.shape[0], -1])
            data = tf.reduce_mean(tf.abs(data), axis=-1, keepdims=True)
            r = cdtipy.fitting.MatrixInversion(data.numpy(), b_vectors, mask=mask.numpy())
            r.fit(disable_progressbar=True)
            temp = tf.cast(tf.concat(r.get_result(dimensions=1), axis=1), tf.float32)
            temp = cdtipy.utils.fill_dense_map(mask, temp)
            lsq.append(temp.numpy())
            progress_bar.update(1)
            
    return gt, predictions, lsq, masks_


def find_outliers(data, m=20):
    """ Cleans the given data from outliers that are larger than m times the median of 
    all absolute difference of all data points to the median data
    """
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/mdev if mdev else 0.
    outlier_mask = s > m
    return outlier_mask